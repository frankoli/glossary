# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-07 22:26+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n "
"<= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#: glossary/models.py:113
msgid "Term have the same language and concept as another one."
msgstr "Termín má stejný jazyk a koncept jako jiný"

#: glossary/models.py:119
msgid ""
"Term may not have any tag, but have same name and language as another one."
msgstr "Termín nemá žádný tag, ale má stejné jméno a jazyk jako jiný"

#: glossary/models.py:124
msgid "Term have the same name, language and concept as another one."
msgstr "Termín má stejné jméno jazyk a koncept jako jiný"

#: glossary/models.py:138
msgid "Term have the same name, language and tags as another one."
msgstr "Termín má stejný název jazyk a tagy jako jiný"

#: glossary/models.py:178
msgid "Definition should have some term."
msgstr "Definice by měla mít nějaký termín"

#: glossary/web/templates/glossary/components/fields/abstract_field.html:4
msgid "Abstract of Gloss"
msgstr "Abstrakt pro glosu "

#: glossary/web/templates/glossary/components/fields/abstract_field.html:13
msgid "This filed is optional"
msgstr "Toto pole je nepovinné"

#: glossary/web/templates/glossary/components/fields/add_tag_filed.html:4
msgid "Tag already exists"
msgstr "Tag již existuje"

#: glossary/web/templates/glossary/components/fields/add_tag_filed.html:7
msgid "Tag can not be created"
msgstr "Tag nemůže být vytvořen"

#: glossary/web/templates/glossary/components/fields/add_tag_filed.html:12
msgid "Add new tag"
msgstr "Přidat nový tag"

#: glossary/web/templates/glossary/components/fields/add_tag_filed.html:13
msgid "Create"
msgstr "Přidat"

#: glossary/web/templates/glossary/components/fields/language_radio_field.html:5
msgid "Chose language:"
msgstr "Vyberte jazyk:"

#: glossary/web/templates/glossary/components/fields/language_radio_field.html:8
msgid "You should chose another language"
msgstr "Zkuste vybrat jiný jazyk"

#: glossary/web/templates/glossary/components/fields/tag_filed.html:6
msgid "Chose tags:"
msgstr "Vyberte tagy:"

#: glossary/web/templates/glossary/components/search_bar.html:13
#: glossary/web/templates/glossary/components/search_bar.html:15
msgid "Search"
msgstr "Vyhledat"

#: glossary/web/templates/glossary/pages/editor.html:12
#: glossary/web/templates/glossary/pages/gloss_page.html:11
#: glossary/web/templates/glossary/pages/main.html:14
msgid "Main Page"
msgstr "Hlavní stránka"

#: glossary/web/templates/glossary/pages/editor.html:28
msgid "Add tags or use different ones"
msgstr "Přidejte tagy nebo použijte jiné"

#: glossary/web/templates/glossary/pages/editor.html:46
msgid "Use different name"
msgstr "Použijte jiné jméno"

#: glossary/web/templates/glossary/pages/editor.html:50
msgid "Name of Gloss:"
msgstr "Jméno glosy:"

#: glossary/web/templates/glossary/pages/editor.html:53
msgid "Input Gloss name"
msgstr "Vložte název glosy"

#: glossary/web/templates/glossary/pages/editor.html:61
msgid "Save"
msgstr "Uložit"

#: glossary/web/templates/glossary/pages/error_page.html:4
#, fuzzy
#| msgid "Something went wrong"
msgid "Something went wrong 😔 "
msgstr "Něco se nepovedlo 😔"

#: glossary/web/templates/glossary/pages/gloss_page.html:14
#: glossary/web/templates/glossary/pages/main.html:17
msgid "Add New"
msgstr "Přidat nový"

#: glossary/web/templates/glossary/pages/main.html:30
#: glossary/web/templates/glossary/pages/main.html:33
msgid "all tags"
msgstr "všechny tagy"

#: glossary/web/templates/glossary/pages/main.html:31
#: glossary/web/templates/glossary/pages/main.html:34
msgid "any tag"
msgstr "alespoň jeden tag"

#: glossary/web/templatetags/objects_access.py:32 glossary/web/views.py:31
#: glossary/web/views.py:174 glossary/web/views.py:336
#: glossary/web/views.py:367
msgid "> Definition doesnt exists yet !"
msgstr "> Definice ještě neexistuje  !"

#: glossary/web/views.py:227
msgid "Gloss name should not be empty."
msgstr "Jméno glosy by nemělo být prázdné."

#: glossary/web/views.py:388
msgid "This gloss can not be deleted"
msgstr "Tato glosa nemůže být smazána"

#: glossary/web/views.py:397
msgid "Something went wrong, object can not be deleted"
msgstr "Něco se nepovedlo, objekt nemůže být smazán"
