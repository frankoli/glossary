""" Database models, there are all models for glossary database"""
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.db import models

import glossary.lemmatize_functions as lemmatize
from glossary.settings import ABSTR_LENGTH, MAX_LENGTH


class Concept(models.Model):
    """Represents Concept of gloss.
    Connects term and their meaning over the id"""

    def __str__(self):
        return str(self.id)


class User(models.Model):
    """Represents User"""
    username = models.CharField(max_length=30, unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(unique=True)
    password = models.TextField()  # MUST use hash function to store hashed passwords here
    birth_date = models.DateTimeField(null=True, blank=True)

    @property
    def full_name(self):
        """Returns the user's full name."""
        return f"{self.first_name}, {self.last_name}."

    def __str__(self):
        field_values = []
        for field in self._meta.get_fields():
            field_values.append(str(getattr(self, field.name, '')))
        return ', '.join(field_values)


class Language(models.Model):
    """Represents language of gloss."""
    LANGUAGE_CHOICE = (
        ('cz', 'Czech'),
        ('en', 'English'),
        ('es', 'Spanish')
        # could be added more languages
    )
    name = models.CharField(max_length=2, choices=LANGUAGE_CHOICE, primary_key=True, default='cz')

    def save(self, *args, **kwargs):
        """ override language save method
            check that name is one of choice
        """
        if not [choice for choice in self.LANGUAGE_CHOICE if self.name in choice]:
            raise TypeError(_('Expected a value of type GlossaryLanguageNameChoices'))
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class TagType(models.Model):
    """Represents type of tag. For future tag improvements"""
    name = models.CharField("tag type name", max_length=MAX_LENGTH, primary_key=True)

    def __str__(self):
        return self.name


class Tag(models.Model):
    """Represents tag for gloss.
    Tag is the area which the gloss belongs."""
    name = models.CharField("tag name", max_length=MAX_LENGTH, primary_key=True)
    tag_type = models.ForeignKey(TagType, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Term(models.Model):
    """Represents name of gloss."""
    name = models.CharField("term name", max_length=MAX_LENGTH, unique=False)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    concept = models.ForeignKey(Concept, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag, blank=True)

    force = False

    def __clean_mess(self, old):
        if not old:
            super().delete()

    def force_save(self, *args, **kwargs):
        """ save without check but set force tag to True
            object will be destroy if will not be correct
            after adding tags"""
        self.force = True
        super().save(*args, **kwargs)

    def save(self, *args, **kwargs):
        """
        This method override save method.
        we can not create two objects with same name, language and concept.
        we can not create two objects with same language and concept
        If two objects have same name and language, they must have different tags.
        """
        old = False
        if self.id and not self.force:
            old = True
        else:
            self.force = False
            super().save(*args, **kwargs)

        terms = Term.objects.filter(language=self.language, concept=self.concept) \
            .exclude(id=self.id)
        if terms.count() != 0:
            self.__clean_mess(old)
            raise ValidationError(_('Term have the same language and concept as another one.'),
                                  code='lang_conc')
        terms = Term.objects.filter(name=self.name, language=self.language).exclude(id=self.id)
        if self.tags.count() == 0 and terms.count() != 0:
            # term doesnt have tag but have same name and language as another
            self.__clean_mess(old)
            raise ValidationError(_('Term may not have any tag, but have same name and language'
                                    ' as another one.'), code='no_tag')
        for i in terms:  # iterate by all terms
            if i.concept == self.concept:  # if the concept is the same, object wont be created
                self.__clean_mess(old)
                raise ValidationError(_('Term have the same name, language and concept as another'
                                        ' one.'), code='name_lang_conc')
            # if the number of tags is the same
            if self.tags.count() == i.tags.count():
                count = 0
                # find out if all tags are the same
                for tag in self.tags.all():
                    if i.tags.contains(tag):
                        count += 1
                    else:
                        break
                # if all tags are the same
                if count == self.tags.count():
                    self.__clean_mess(old)
                    raise ValidationError(_('Term have the same name, language and tags'
                                            ' as another one.'), code='name_lang_tag')
        if self.id:
            super().save(*args, **kwargs)

    def delete(self, using=None, keep_parents=False):
        """ After deletion the term definition should be deleted too"""
        definition = Definition.objects.filter(language=self.language, concept=self.concept)
        for i in definition:
            i.delete()
        terms = Term.objects.filter(concept_id=self.concept.id)
        if len(terms) == 1:
            Concept.objects.filter(pk=self.concept.id).delete()
        super().delete()

    def __str__(self):
        return f'{self.name} {self.language} {self.concept}'


class Definition(models.Model):
    """Represents tag for gloss.
    Tag is the area which the gloss belongs."""
    abstract = models.TextField(blank=True, max_length=ABSTR_LENGTH)
    file = models.TextField()
    # concept = models.OneToOneField(Concept, primary_key=True, on_delete=models.CASCADE)
    concept = models.ForeignKey(Concept, on_delete=models.CASCADE)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    user = models.ManyToManyField(User, blank=True)
    active = models.BooleanField(default=True)

    lemm_tags = []

    def save(self, *args, **kwargs):
        """
        This method override save method.
        Definition should have had same concept and language as some term.
        """
        terms = Term.objects.filter(language=self.language, concept=self.concept)

        if terms.count() == 0:
            raise ValidationError(_('Definition should have some term.'), code='no_term')
        self.lemm_tags = lemmatize.get_main_data(self.language.name, self.file)

        super().save(*args, **kwargs)

    def __str__(self):
        return self.file
