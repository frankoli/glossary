from django.test import TestCase

from glossary.models import Concept, Language, Tag, TagType, Term, Definition


class TestModels(TestCase):

    def setUp(self):
        self.concept = Concept.objects.create()
        self.concept2 = Concept.objects.create()
        self.language = Language.objects.create(name="cz")
        self.languageEn = Language.objects.create(name="en")
        self.tag_type = TagType.objects.create(name="basic")

    def test_term_create(self):
        "test create term"
        self.assertEqual(Term.objects.all().count(), 0)
        term1 = Term(name="test1", language=self.language, concept=self.concept)
        term1.save()
        self.assertEqual(Term.objects.all().count(), 1)
        new_term = Term.objects.all().first()
        self.assertFalse(new_term.force)
        self.assertEqual(new_term.name, "test1")
        self.assertFalse(new_term.tags.exists())

    def test_term_with_tag_create(self):
        "Test create term with tags"
        tag = Tag.objects.create(name="test", tag_type=self.tag_type)
        term1 = Term(name="test1", language=self.language,
                     concept=self.concept)
        term1.save()
        term1.tags.add(tag)
        term1.save()
        self.assertEqual(Term.objects.all().count(), 1)
        new_term = Term.objects.all().first()
        self.assertFalse(new_term.force)
        self.assertEqual(new_term.name, "test1")
        self.assertTrue(new_term.tags.exists())

    def test_two_term_create(self):
        """test that we can not create two terms with same name and lang
            without tag
        """

        tag = Tag.objects.create(name="test", tag_type=self.tag_type)
        term1 = Term(name="test1", language=self.language,
                     concept=self.concept)
        term2 = Term(name="test1", language=self.language,
                     concept=self.concept2)
        term1.save()
        exception = False
        try:
            term2.save()
        except Exception as e:
            exception = True
        term2.force_save()
        self.assertTrue(term2.force)
        term2.tags.add(tag)
        term2.save()

        self.assertEqual(Term.objects.all().count(), 2)
        new_term = Term.objects.all()
        self.assertTrue(exception)
        for i in new_term:
            self.assertFalse(i.force)
            self.assertEqual(i.name, "test1")

    def test_create_two_terms_same_concept(self):
        term1 = Term(name="test1", language=self.language,
                     concept=self.concept)
        term2 = Term(name="test2", language=self.languageEn,
                     concept=self.concept)
        term1.save()
        term2.save()
        self.assertEqual(Term.objects.all().count(), 2)

    def test_create_definition(self):
        """To create Definition must exist some term with the same lang and concept"""
        term1 = Term(name="test1", language=self.language,
                     concept=self.concept)
        definition1 = Definition(file="ahoj ja jsem test", language=self.language,
                                 concept=self.concept)
        exception = False
        try:
            definition1.save()
        except Exception as e:
            exception = True
        self.assertTrue(exception)
        term1.save()
        definition1.save()
        self.assertEqual(Definition.objects.all().count(), 1)
        new_def = Definition.objects.all().first()
        self.assertEqual(new_def.file, "ahoj ja jsem test")

    def test_delete_term(self):
        """Test that if we delete term, then will be deleted
         definition and if concept doesnt have any other term
         it will be also deleted"""
        term1 = Term(name="test1", language=self.language,
                     concept=self.concept)
        term2 = Term(name="test2", language=self.languageEn,
                     concept=self.concept)
        term1.save()
        term2.save()
        self.assertEqual(Term.objects.all().count(), 2)
        definition1 = Definition(file="ahoj ja jsem test", language=self.language,
                     concept=self.concept)
        definition1.save()
        self.assertEqual(Definition.objects.all().count(), 1)
        self.assertEqual(Concept.objects.all().count(), 2)
        term1.delete()
        self.assertEqual(Term.objects.all().count(), 1)
        self.assertEqual(Definition.objects.all().count(), 0)
        self.assertEqual(Concept.objects.all().count(), 2)
        term2.delete()
        self.assertEqual(Term.objects.all().count(), 0)
        self.assertEqual(Definition.objects.all().count(), 0)
        self.assertEqual(Concept.objects.all().count(), 1)

    def test_create_tag(self):
        tag = Tag(name="new_tag", tag_type=self.tag_type)
        tag.save()
        self.assertEqual(Tag.objects.all().count(), 1)
        new_term = Tag.objects.all().first()
        self.assertEqual(new_term.name, "new_tag")

    def test_delete_tag(self):
        tag = Tag.objects.create(name="test", tag_type=self.tag_type)
        term1 = Term(name="test1", language=self.language,
                     concept=self.concept)
        term1.save()
        term1.tags.add(tag)
        term1.save()
        self.assertEqual(Term.objects.all().count(), 1)
        tag.delete()
        self.assertEqual(Term.objects.all().count(), 1)
        self.assertEqual(Tag.objects.all().count(), 0)
        new_term = Term.objects.all().first()
        self.assertFalse(new_term.tags.exists())

    def test_delete_tag_type(self):
        tag = Tag.objects.create(name="test", tag_type=self.tag_type)
        term1 = Term(name="test1", language=self.language,
                     concept=self.concept)
        term1.save()
        term1.tags.add(tag)
        term1.save()
        self.assertEqual(Term.objects.all().count(), 1)
        self.tag_type.delete()
        self.assertEqual(Term.objects.all().count(), 1)
        self.assertEqual(Tag.objects.all().count(), 0)
        new_term = Term.objects.all().first()
        self.assertFalse(new_term.tags.exists())

    def test_create_lang(self):
        language = Language(name="neco nahodneho")
        exception = False
        try:
            language.save()
        except Exception as e:
            exception = True
        self.assertTrue(exception)
        self.assertEqual(Language.objects.all().count(), 2)

        language = Language(name="es")
        language.save()
        self.assertEqual(Language.objects.all().count(), 3)









