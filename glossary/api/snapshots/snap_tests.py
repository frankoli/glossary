# pylint: skip-file
# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['CreateTests::test_create_concept 1'] = {
    'data': {
        'createConcept': {
            'concept': {
                'id': '1'
            }
        }
    }
}

snapshots['CreateTests::test_create_definition 1'] = {
    'data': {
        'createDefinition': {
            'definition': None
        }
    },
    'errors': [
        {
            'locations': [
                {
                    'column': 34,
                    'line': 3
                }
            ],
            'message': "['Definition should have some term.']",
            'path': [
                'createDefinition',
                'definition'
            ]
        }
    ]
}

snapshots['CreateTests::test_create_definition 2'] = {
    'data': {
        'createDefinition': {
            'definition': {
                'abstract': 'abstract',
                'file': 'some file',
                'language': {
                    'name': 'EN'
                }
            }
        }
    }
}

snapshots['CreateTests::test_create_gloss 1'] = {
    'data': {
        'createGloss': {
            'definition': {
                'file': 'vec na cteni'
            },
            'term': {
                'name': 'kniha',
                'tags': [
                    {
                        'name': 'test'
                    }
                ]
            }
        }
    }
}

snapshots['CreateTests::test_create_language 1'] = {
    'data': {
        'createLanguage': {
            'language': {
                'name': 'CZ'
            }
        }
    }
}

snapshots['CreateTests::test_create_tag 1'] = {
    'data': {
        'createTag': {
            'tag': {
                'name': 'test'
            }
        }
    }
}

snapshots['CreateTests::test_create_tag_type 1'] = {
    'data': {
        'createTagType': {
            'tagType': {
                'name': 'basic'
            }
        }
    }
}

snapshots['CreateTests::test_create_term 1'] = {
    'data': {
        'createTerm': {
            'term': {
                'language': {
                    'name': 'EN'
                },
                'name': 'cat',
                'tags': [
                    {
                        'name': 'szz'
                    },
                    {
                        'name': 'test'
                    }
                ]
            }
        }
    }
}

snapshots['CreateTests::test_no_two_languages 1'] = {
    'data': {
        'createLanguage': {
            'language': {
                'name': 'CZ'
            }
        }
    }
}

snapshots['CreateTests::test_no_two_languages 2'] = {
    'data': {
        'createLanguage': None
    },
    'errors': [
        {
            'locations': [
                {
                    'column': 10,
                    'line': 1
                }
            ],
            'message': 'UNIQUE constraint failed: glossary_language.name',
            'path': [
                'createLanguage'
            ]
        }
    ]
}

snapshots['CreateTests::test_tag_without_type 1'] = {
    'data': {
        'createTag': None
    },
    'errors': [
        {
            'locations': [
                {
                    'column': 10,
                    'line': 1
                }
            ],
            'message': 'TagType matching query does not exist.',
            'path': [
                'createTag'
            ]
        }
    ]
}

snapshots['DeleteTests::test_delete_concept 1'] = {
    'data': {
        'deleteConcept': {
            'concept': {
                'id': '1'
            }
        }
    }
}

snapshots['DeleteTests::test_delete_definition 1'] = {
    'data': {
        'deleteDefinition': {
            'definition': {
                'file': 'heloo testcz'
            }
        }
    }
}

snapshots['DeleteTests::test_delete_gloss 1'] = {
    'data': {
        'deleteGloss': {
            'term': {
                'name': 'Testcz'
            }
        }
    }
}

snapshots['DeleteTests::test_delete_gloss 2'] = {
    'data': {
        'deleteGloss': {
            'term': {
                'name': 'Testen'
            }
        }
    }
}

snapshots['DeleteTests::test_delete_language 1'] = {
    'data': {
        'deleteLanguage': {
            'language': {
                'name': 'EN'
            }
        }
    }
}

snapshots['DeleteTests::test_delete_tag 1'] = {
    'data': {
        'deleteTag': {
            'tag': {
                'name': 'test'
            }
        }
    }
}

snapshots['DeleteTests::test_delete_tag_type 1'] = {
    'data': {
        'deleteTagType': {
            'tagType': {
                'name': 'basic'
            }
        }
    }
}

snapshots['DeleteTests::test_delete_term 1'] = {
    'data': {
        'deleteTerm': {
            'term': None
        }
    }
}

snapshots['GetTests::test_definition 1'] = {
    'data': {
        'definition': {
            'file': 'heloo testcz',
            'language': {
                'name': 'CZ'
            }
        }
    }
}

snapshots['GetTests::test_definitions 1'] = {
    'data': {
        'definitions': [
            {
                'file': 'heloo testcz',
                'language': {
                    'name': 'CZ'
                }
            },
            {
                'file': 'heloo testen',
                'language': {
                    'name': 'EN'
                }
            },
            {
                'file': 'Pes domácí (Canis lupus f. familiaris) je největší domestikovaná šelma a jedno z nejstarších domestikovaných zvířat vůbec, provázející člověka minimálně 14 tisíc let.[1] Obecně se předpokládá, že se jedná o zdomácnělého a umělým výběrem změněného vlka obecného. Celosvětová populace psů je odhadována na 500 miliónů,[2] přičemž toulavých a opuštěných psů je minimálně 370 miliónů,[2] nezisková organizace 600million odhaduje počet jen toulavých psů na světě právě na 600 miliónů zvířat.',
                'language': {
                    'name': 'CZ'
                }
            },
            {
                'file': "The dog or domestic dog (Canis familiaris[4][5] or Canis lupus familiaris[5]) is a domesticated descendant of the wolf which is characterized by an upturning tail. The dog is derived from an ancient, extinct wolf,[6][7] and the modern wolf is the dog's nearest living relative.[8] The dog was the first species to be domesticated,[9][8] by hunter–gatherers over 15,000 years ago,[7] before the development of agriculture.[1]Due to their long association with humans, dogs have expanded to a large number of domestic individuals[10] and gained the ability to thrive on a starch-rich diet that would be inadequate for other canids.[11] Over the millennia, dogs became uniquely adapted to human behavior, and the human-canine bond has been a topic of frequent study.",
                'language': {
                    'name': 'EN'
                }
            }
        ]
    }
}

snapshots['GetTests::test_find 1'] = {
    'data': {
        'find': [
            {
                'term': {
                    'name': 'Dog'
                }
            },
            {
                'term': {
                    'name': 'Pes'
                }
            },
            {
                'term': {
                    'name': 'Be'
                }
            }
        ]
    }
}

snapshots['GetTests::test_find_exact 1'] = {
    'data': {
        'find': [
            {
                'term': {
                    'name': 'Dog'
                }
            },
            {
                'term': {
                    'name': 'Be'
                }
            }
        ]
    }
}

snapshots['GetTests::test_find_in 1'] = {
    'data': {
        'findIn': [
            {
                'definition': [
                    {
                        'file': "The dog or domestic dog (Canis familiaris[4][5] or Canis lupus familiaris[5]) is a domesticated descendant of the wolf which is characterized by an upturning tail. The dog is derived from an ancient, extinct wolf,[6][7] and the modern wolf is the dog's nearest living relative.[8] The dog was the first species to be domesticated,[9][8] by hunter–gatherers over 15,000 years ago,[7] before the development of agriculture.[1]Due to their long association with humans, dogs have expanded to a large number of domestic individuals[10] and gained the ability to thrive on a starch-rich diet that would be inadequate for other canids.[11] Over the millennia, dogs became uniquely adapted to human behavior, and the human-canine bond has been a topic of frequent study."
                    }
                ],
                'term': {
                    'name': 'Dog'
                }
            },
            {
                'definition': [
                    {
                        'file': 'Pes domácí (Canis lupus f. familiaris) je největší domestikovaná šelma a jedno z nejstarších domestikovaných zvířat vůbec, provázející člověka minimálně 14 tisíc let.[1] Obecně se předpokládá, že se jedná o zdomácnělého a umělým výběrem změněného vlka obecného. Celosvětová populace psů je odhadována na 500 miliónů,[2] přičemž toulavých a opuštěných psů je minimálně 370 miliónů,[2] nezisková organizace 600million odhaduje počet jen toulavých psů na světě právě na 600 miliónů zvířat.'
                    }
                ],
                'term': {
                    'name': 'Pes'
                }
            }
        ]
    }
}

snapshots['GetTests::test_find_in_cz 1'] = {
    'data': {
        'findIn': [
            {
                'definition': [
                    {
                        'file': 'Pes domácí (Canis lupus f. familiaris) je největší domestikovaná šelma a jedno z nejstarších domestikovaných zvířat vůbec, provázející člověka minimálně 14 tisíc let.[1] Obecně se předpokládá, že se jedná o zdomácnělého a umělým výběrem změněného vlka obecného. Celosvětová populace psů je odhadována na 500 miliónů,[2] přičemž toulavých a opuštěných psů je minimálně 370 miliónů,[2] nezisková organizace 600million odhaduje počet jen toulavých psů na světě právě na 600 miliónů zvířat.'
                    }
                ],
                'term': {
                    'name': 'Pes'
                },
                'word': 'psa'
            },
            {
                'definition': [
                ],
                'term': {
                    'name': 'být'
                },
                'word': 'bude'
            }
        ]
    }
}

snapshots['GetTests::test_find_in_en 1'] = {
    'data': {
        'findIn': [
            {
                'definition': [
                    {
                        'file': "The dog or domestic dog (Canis familiaris[4][5] or Canis lupus familiaris[5]) is a domesticated descendant of the wolf which is characterized by an upturning tail. The dog is derived from an ancient, extinct wolf,[6][7] and the modern wolf is the dog's nearest living relative.[8] The dog was the first species to be domesticated,[9][8] by hunter–gatherers over 15,000 years ago,[7] before the development of agriculture.[1]Due to their long association with humans, dogs have expanded to a large number of domestic individuals[10] and gained the ability to thrive on a starch-rich diet that would be inadequate for other canids.[11] Over the millennia, dogs became uniquely adapted to human behavior, and the human-canine bond has been a topic of frequent study."
                    }
                ],
                'term': {
                    'name': 'Dog'
                },
                'word': 'Dogs'
            },
            {
                'definition': [
                ],
                'term': {
                    'name': 'Be'
                },
                'word': 'are'
            }
        ]
    }
}

snapshots['GetTests::test_find_lemmatize 1'] = {
    'data': {
        'find': [
            {
                'term': {
                    'name': 'Dog'
                }
            },
            {
                'term': {
                    'name': 'Pes'
                }
            },
            {
                'term': {
                    'name': 'Be'
                }
            }
        ]
    }
}

snapshots['GetTests::test_find_lemmatize_en 1'] = {
    'data': {
        'find': [
            {
                'term': {
                    'name': 'Dog'
                }
            },
            {
                'term': {
                    'name': 'Be'
                }
            }
        ]
    }
}

snapshots['GetTests::test_gloss 1'] = {
    'data': {
        'gloss': {
            'term': {
                'concept': {
                    'id': '1'
                },
                'language': {
                    'name': 'CZ'
                },
                'name': 'Testcz'
            }
        }
    }
}

snapshots['GetTests::test_glosses 1'] = {
    'data': {
        'glosses': [
            {
                'term': {
                    'concept': {
                        'id': '1'
                    },
                    'language': {
                        'name': 'EN'
                    },
                    'name': 'Testen'
                }
            },
            {
                'term': {
                    'concept': {
                        'id': '1'
                    },
                    'language': {
                        'name': 'CZ'
                    },
                    'name': 'Testcz'
                }
            },
            {
                'term': {
                    'concept': {
                        'id': '2'
                    },
                    'language': {
                        'name': 'CZ'
                    },
                    'name': 'Pes'
                }
            },
            {
                'term': {
                    'concept': {
                        'id': '3'
                    },
                    'language': {
                        'name': 'CZ'
                    },
                    'name': 'být'
                }
            }
        ]
    }
}

snapshots['GetTests::test_glosses 2'] = {
    'data': {
        'glosses': [
            {
                'term': {
                    'concept': {
                        'id': '1'
                    },
                    'language': {
                        'name': 'CZ'
                    },
                    'name': 'Testcz'
                }
            }
        ]
    }
}

snapshots['GetTests::test_glosses_tags 1'] = {
    'data': {
        'glosses': [
            {
                'term': {
                    'concept': {
                        'id': '1'
                    },
                    'language': {
                        'name': 'EN'
                    },
                    'name': 'Testen',
                    'tags': [
                        {
                            'name': 'test'
                        }
                    ]
                }
            },
            {
                'term': {
                    'concept': {
                        'id': '2'
                    },
                    'language': {
                        'name': 'EN'
                    },
                    'name': 'Dog',
                    'tags': [
                        {
                            'name': 'animal'
                        },
                        {
                            'name': 'test'
                        }
                    ]
                }
            },
            {
                'term': {
                    'concept': {
                        'id': '2'
                    },
                    'language': {
                        'name': 'CZ'
                    },
                    'name': 'Pes',
                    'tags': [
                        {
                            'name': 'animal'
                        }
                    ]
                }
            }
        ]
    }
}

snapshots['GetTests::test_glosses_tags 2'] = {
    'data': {
        'glosses': [
            {
                'term': {
                    'concept': {
                        'id': '2'
                    },
                    'language': {
                        'name': 'EN'
                    },
                    'name': 'Dog',
                    'tags': [
                        {
                            'name': 'animal'
                        },
                        {
                            'name': 'test'
                        }
                    ]
                }
            }
        ]
    }
}

snapshots['GetTests::test_language 1'] = {
    'data': {
        'language': {
            'name': 'CZ'
        }
    }
}

snapshots['GetTests::test_languages 1'] = {
    'data': {
        'languages': [
            {
                'name': 'CZ'
            },
            {
                'name': 'EN'
            }
        ]
    }
}

snapshots['GetTests::test_tag 1'] = {
    'data': {
        'tag': {
            'name': 'animal',
            'tagType': {
                'name': 'basic'
            }
        }
    }
}

snapshots['GetTests::test_tag_type 1'] = {
    'data': {
        'tagType': {
            'name': 'basic'
        }
    }
}

snapshots['GetTests::test_tag_types 1'] = {
    'data': {
        'tagTypes': [
            {
                'name': 'basic'
            }
        ]
    }
}

snapshots['GetTests::test_tags 1'] = {
    'data': {
        'tags': [
            {
                'name': 'test'
            },
            {
                'name': 'animal'
            }
        ]
    }
}

snapshots['GetTests::test_term 1'] = {
    'data': {
        'term': {
            'name': 'Testen'
        }
    }
}

snapshots['GetTests::test_term_by_name 1'] = {
    'data': {
        'termByName': [
            {
                'language': {
                    'name': 'CZ'
                },
                'name': 'Pes'
            }
        ]
    }
}

snapshots['GetTests::test_term_by_name_contains 1'] = {
    'data': {
        'termByName': [
            {
                'language': {
                    'name': 'EN'
                },
                'name': 'Testen'
            },
            {
                'language': {
                    'name': 'CZ'
                },
                'name': 'Testcz'
            }
        ]
    }
}

snapshots['GetTests::test_term_by_name_starts 1'] = {
    'data': {
        'termByName': [
        ]
    }
}

snapshots['GetTests::test_term_by_name_starts 2'] = {
    'data': {
        'termByName': [
            {
                'language': {
                    'name': 'EN'
                },
                'name': 'Testen'
            },
            {
                'language': {
                    'name': 'CZ'
                },
                'name': 'Testcz'
            }
        ]
    }
}

snapshots['GetTests::test_terms 1'] = {
    'data': {
        'terms': [
            {
                'language': {
                    'name': 'EN'
                },
                'name': 'Testen'
            },
            {
                'language': {
                    'name': 'CZ'
                },
                'name': 'Testcz'
            },
            {
                'language': {
                    'name': 'EN'
                },
                'name': 'Dog'
            },
            {
                'language': {
                    'name': 'CZ'
                },
                'name': 'Pes'
            },
            {
                'language': {
                    'name': 'EN'
                },
                'name': 'Be'
            },
            {
                'language': {
                    'name': 'CZ'
                },
                'name': 'být'
            }
        ]
    }
}

snapshots['UpdateTests::test_update_definition 1'] = {
    'data': {
        'updateDefinition': {
            'definition': {
                'abstract': 'new abstract',
                'file': 'some'
            }
        }
    }
}

snapshots['UpdateTests::test_update_definition_term 1'] = {
    'data': {
        'updateDefinition': {
            'definition': None
        }
    },
    'errors': [
        {
            'locations': [
                {
                    'column': 62,
                    'line': 1
                }
            ],
            'message': "['Definition should have some term.']",
            'path': [
                'updateDefinition',
                'definition'
            ]
        }
    ]
}

snapshots['UpdateTests::test_update_definition_term 2'] = {
    'data': {
        'updateDefinition': {
            'definition': {
                'abstract': 'abstract',
                'concept': {
                    'id': '2'
                },
                'file': 'test 123',
                'language': {
                    'name': 'EN'
                }
            }
        }
    }
}

snapshots['UpdateTests::test_update_gloss 1'] = {
    'data': {
        'updateGloss': {
            'definition': {
                'abstract': 'new abstract',
                'file': 'new file'
            },
            'term': {
                'name': 'new name',
                'tags': [
                ]
            }
        }
    }
}

snapshots['UpdateTests::test_update_gloss_language 1'] = {
    'data': {
        'updateGloss': {
            'definition': {
                'abstract': 'abstract',
                'file': 'test 123',
                'language': {
                    'name': 'EN'
                }
            },
            'term': {
                'language': {
                    'name': 'EN'
                },
                'name': 'test',
                'tags': [
                    {
                        'name': 'test'
                    }
                ]
            }
        }
    }
}

snapshots['UpdateTests::test_update_tag 1'] = {
    'data': {
        'updateTag': {
            'tag': {
                'name': 'pst'
            }
        }
    }
}

snapshots['UpdateTests::test_update_tag 2'] = {
    'data': {
        'updateTag': {
            'tag': {
                'name': 'pst',
                'tagType': {
                    'name': 'admin'
                }
            }
        }
    }
}

snapshots['UpdateTests::test_update_tag_type 1'] = {
    'data': {
        'updateTagType': {
            'tagType': {
                'name': 'admin'
            }
        }
    }
}

snapshots['UpdateTests::test_update_term 1'] = {
    'data': {
        'updateTerm': {
            'term': {
                'name': 'some',
                'tags': [
                ]
            }
        }
    }
}
