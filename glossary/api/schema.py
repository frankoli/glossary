# pylint: disable=too-few-public-methods
# pylint: disable=unused-argument
# pylint: disable=no-self-use
"""
This file contains Graphene declaration of
mutations and queries in GraphQL Glossary API.

There are defined GrpahQL object types which are mapping models from glossary.models
"""
import logging
import os

import graphene

from django.core.exceptions import ValidationError

from graphene_django import DjangoObjectType
from simplemma import text_lemmatizer

import simplemma

from glossary.lemmatize_functions import get_langdata, get_main_data
from glossary.models import Concept, User, Language, TagType, Tag, Term, Definition


if not os.environ.get('DOCS', ''):
    LANG_DATA = get_langdata()

logger = logging.getLogger(__name__)


class ConceptType(DjangoObjectType):
    """
    This class is concept of gloss. Based on Concept from models.
    """

    class Meta:
        model = Concept
        fields = "__all__"


class UserType(DjangoObjectType):
    """
    This is graphql representation of User from models
    """

    class Meta:
        model = User
        fields = "__all__"


class LanguageType(DjangoObjectType):
    """
    This is graphql representation of Language from models
     """

    class Meta:
        model = Language
        fields = "__all__"


class TermType(DjangoObjectType):
    """
    This is graphql representation of term from models
    """

    class Meta:
        model = Term
        fields = ("id", "name", "language", "concept", "tags")


class TagTypeType(DjangoObjectType):
    """
    This is graphql representation of type of tag from models
    """

    class Meta:
        model = TagType
        fields = "__all__"


class TagTagType(DjangoObjectType):
    """
    This is graphql representation of tag for term from models
    """

    class Meta:
        model = Tag
        fields = ("name", "tag_type")


class DefinitionType(DjangoObjectType):
    """
    This is graphql representation of definition from models
    """

    class Meta:
        model = Definition
        fields = ("id", "file", "abstract", "concept", "language", "user")


class GlossType(graphene.ObjectType):
    """
    This is graphql representation of whole gloss,
     which is created by term and definition
     """
    term = graphene.Field(TermType)
    definition = graphene.List(DefinitionType)


class GlossTextType(graphene.ObjectType):
    """
     This is graphql representation of whole gloss with word.
     gloss is created by term and definition.
     word is word which are paired with gloss as result of  query findIn
     """
    term = graphene.Field(TermType)
    word = graphene.String(description="connected word with gloss")
    definition = graphene.List(DefinitionType)


def load_langdata(language):
    """
    Check if langdata are loaded for specific language. And if not load it.

    :param language: for langdata
    """
    global LANG_DATA
    if LANG_DATA.get(language) is None:
        LANG_DATA = get_langdata()


def get_terms(explicit, language=None, concept=None, name=None, tags=None):
    """
    return terms filtered by parameters

    :param explicit: if we search for exact match
    :param language: in which term should be
    :param concept: which should term has
    :param name: for search
    :param tags: which should term has
    :return: filtered terms
    """
    if language is None and concept is None and name is None and tags is None:
        return Term.objects.all()

    tag_set = []
    if tags is not None:
        tag_set = set_tags(tags)
    if explicit is True:
        terms = Term.objects.all()
        if language is not None:
            terms = terms.filter(language__name=language)
        if concept is not None:
            terms = terms.filter(concept_id=concept)
        if name is not None:
            terms = terms.filter(name__icontains=name)
        if tags is not None:
            for tag in tag_set:
                terms = terms.filter(tags__name__iexact=tag.name)
        return terms

    terms = Term.objects.filter(language__name=language)
    terms = Term.objects.filter(concept_id=concept).union(terms)
    if name is not None:
        terms = Term.objects.filter(name__icontains=name).union(terms)
    for tag in tag_set:
        terms = Term.objects.filter(tags__name__iexact=tag.name).union(terms)
    return terms


def lemm_term_by_lang(lang, searched, exact, term, tags):
    """
    Search terms and tags for specific parameters.

    :param lang: in which we search
    :param searched: word
    :param exact: if we want exact match
    :param term: to add
    :param tags: to add
    :return: terms and tags with added searched objects
    """
    try:
        load_langdata(lang)
    except Exception:
        logger.info("language %s is not defined.", lang)
        return term, tags
    search = simplemma.lemmatize(searched, LANG_DATA[lang])
    logger.info("word: %s was lemmatize to : %s", searched, search)
    if exact:
        term = Term.objects.filter(name__iexact=search, language__name=lang).union(
            term)
        tags = Tag.objects.filter(name__iexact=search).union(tags)
    else:
        term = Term.objects.filter(name__icontains=search, language__name=lang).union(
            term)
        tags = Tag.objects.filter(name__icontains=search).union(tags)
    return term, tags


def find_term_tags(lemmatize, searched, exact, lang):
    """
    Find terms and tags which contains searched word

    :param lemmatize: if shoud be lematize
    :param searched: word
    :param exact: if we want exact match
    :param lang: in which search
    :return: searched tags and terms
    """
    if exact:
        term = Term.objects.filter(name__iexact=searched)
        tags = Tag.objects.filter(name__iexact=searched)
    else:
        term = Term.objects.filter(name__icontains=searched)
        tags = Tag.objects.filter(name__icontains=searched)
    if lemmatize and lang != "":
        term, tags = lemm_term_by_lang(lang=lang, searched=searched,
                                       exact=exact, term=term, tags=tags)
    elif lemmatize:
        for language in Language.objects.all():
            term, tags = lemm_term_by_lang(lang=language.name, searched=searched,
                                           exact=exact, term=term, tags=tags)
    return term, tags


def get_term_result(searched, lemmatize, exact, lang):
    """
     Filter terms and tags by searched expression. Find terms by searched tags.

    :param searched: what was searched
    :param exact: if we search for exact term
    :param lemmatize: if we allow lemmatize
    :return: filtered terms
    """
    term, tags = find_term_tags(lemmatize=lemmatize, searched=searched, exact=exact, lang=lang)
    for i in tags:
        term = Term.objects.filter(tags__name__iexact=i.name).union(term)
    return term


def get_def_result(searched, lemmatize, exact, lang):
    """
    Filter definition by parameters.

    :param searched: what was searched
    :param exact: if we search for exact term
    :param lemmatize: if we allow lemmatize
    :return: filtered definitions
    """
    if exact:
        search = fr'( {searched} |\<{searched}|{searched}\>|{searched}\.)'
        defin = Definition.objects.filter(file__iregex=search).union(
            Definition.objects.filter(abstract__iregex=search)
        )
        return defin

    if lemmatize:
        defin = Definition.objects.filter(abstract__icontains=searched).union(
        )
        if lang != "":
            load_langdata(lang)
            lem_search = simplemma.lemmatize(searched, LANG_DATA[lang])
            defin = defin.union(Definition.objects.filter(abstract__icontains=lem_search,
                                                          language__name=lang)).union(
                Definition.objects.filter(file__icontains=lem_search, language__name=lang))
        else:
            for language in Language.objects.all():
                load_langdata(language.name)
                lem_search = simplemma.lemmatize(searched, LANG_DATA[language.name])
                defin = defin.union(Definition.objects.filter(abstract__icontains=lem_search,
                                                              language__name=language.name)).union(
                    Definition.objects.filter(file__icontains=lem_search,
                                              language__name=language.name))
        return defin
    return Definition.objects.filter(abstract__icontains=searched).union(
        Definition.objects.filter(file__icontains=searched))


def find_match(term, lemmatize, exact, lang):
    """
    Find terms and definition  which are matched with term name and other parameters.
    Find term for each definition and add it to terms set.

    :param term: name of gloss
    :param lemmatize: if term should be lemmatized
    :param exact: if we search for exact term
    :param lang: in which search
    :return: all searched terms
    """
    terms = get_term_result(searched=term, lemmatize=lemmatize, exact=exact, lang=lang)
    defins = get_def_result(searched=term, lemmatize=lemmatize, exact=exact, lang=lang)
    for i in defins:
        try:
            term = Term.objects.filter(concept=i.concept, language=i.language)
            terms = terms.union(term)
        except Exception as exc:
            logger.warning("gloss definition conc: %s, "
                           " lang: %s , id: %s doesnt have term, "
                           "message: %s ", str(i.concept),
                           str(i.language), str(i.id), str(exc))
    return terms


def find_terms(term, language=None, tags=None):
    """
    Find term whit language name and tags

    :param term: name of term
    :param language: of term
    :param tags: of term
    :return: founded terms
    """
    terms = Term.objects.filter(name__iexact=term)
    if language is not None:
        terms = terms.filter(language__name=language)
    if tags is not None:
        terms = terms.filter(tags__in=tags)
    return terms


def create_glosses_array(terms):
    """
    Creates Array of gloss from term and its definition. For each term finds its definition.

    :param terms: to gloss
    :return: array of GlossType
    """
    result = []
    for term in terms:
        defin = Definition.objects.filter(language=term.language,
                                          concept_id=term.concept.id)
        result.append(GlossType(term, defin))
    return result


def user_info(info):
    """
    Returns information about user

    :param info: to parse
    :return: info about user
    """
    if info.context is not None:
        result = "\n\tfrom ip: " + info.context.headers['host'] + "\n\tfrom device: " + \
                 info.context.headers[
                     'user-agent']
        return result
    return ""


def request_info(info):
    """
    Returns parsed info about type query/mutation and body of query/mutation

    :param info: to parse
    :return: parsed info
    """
    if info.context is not None:
        return info.field_name + " was called with body: " + info.context.body.decode("utf-8")
    return ""


def mutate_logg(info):
    """
    Write info to log

    :param info: to be written
    """
    if info.context is not None:
        logger.info(request_info(info) + user_info(info))


def preprocess_text(text):
    """
    Remove all unnecessary elements from text

    :param text: to preprocess
    :return:  preprocessed text
    """
    ignore_tags = ["<code>", "</code>", "<em>", "</em>"] # could be add more
    for i in ignore_tags:
        text = text.replace(i, "")
    filtered_string = ''.join(ch for ch in text if ch.isalnum() or ch == ' ')
    return filtered_string


class Query(graphene.ObjectType):
    """ this query access models in database"""
    # ================================== Find in text ==========================================
    find_in = graphene.List(GlossTextType, text=graphene.String(required=True,
                                                            description="searched text"),
                            language=graphene.String(description="language in which search"),
                            tags=graphene.List(graphene.ID,
                                               description="describe the topic we are looking for"),
                            description="GET all glosses which were found in text. You can "
                                        "specify language and searched topic by tags.")

    def resolve_find_in(self, info, text, language=None, tags=None):
        """
        GET all glosses which were found in text.

        :param info:
        :param text: for search in
        :param language: in which we're searching
        :param tags: in which we're searching
        :return: found glosses
        """

        logger.info("Get all gloses which were found in text. Language: %s, tags %s %s",
                    language, tags, user_info(info))
        if text == "":
            return []
        text = preprocess_text(text)
        words = text.split()
        whole_words = words
        if language is not None:
            try:
                load_langdata(language)
                words = text_lemmatizer(text, LANG_DATA.get(language))
            except Exception:
                logging.info("Lemmatizer for language %s does not exist", language.name)
        glosses = []
        founded_words = []
        count = 0
        for word, lem_word in zip(whole_words, words):
            word = word.replace(".", "")
            if word not in founded_words:
                terms = find_terms(term=lem_word, language=language, tags=tags)
                if len(terms) == 1:
                    founded_words.append(word)
                    term = terms.get()
                    defin = Definition.objects.filter(language=term.language,
                                                      concept_id=term.concept.id)
                    glosses.append(GlossTextType(term, word, defin))
            count += 1
        return glosses

    # ================================== Find ==========================================

    find = graphene.List(GlossType, term=graphene.String(required=True,
                                                         description="wanted term"),
                         exact=graphene.Boolean(True, description="True when we want to search "
                                                                  "for exact term"),
                         lemmatize=graphene.Boolean(True),
                         lang=graphene.String("",
                                              description="Language in which we want to search"),
                         description="GET all glosses where term is present in name, "
                                     "definition or tags. If exact is True we search for the "
                                     "exact term. If exact is False we search for something "
                                     "contains term. If lemmatize is True we use lemmatizer for "
                                     "inflection/plural words.")

    def resolve_find(self, info, term, exact, lemmatize, lang):
        """
        GET all glosses where term is present in name, definition or tags

        :param info:
        :param term: wanted word
        :param exact: True when we want to search for exact term
        :param lemmatize: True when we want lemmatize term
        :param lang: in which are term
        :return: founded glosses
        """
        logger.info("Get all glosses where term: %s is in it, exact: %s, lemmatize: \
                    %s lang: %s %s",
                    term, exact, lemmatize, lang, user_info(info))
        terms = find_match(term=term, lemmatize=lemmatize, exact=exact, lang=lang)
        return create_glosses_array(terms)

    # ================================== Gloss ==========================================
    glosses = graphene.List(GlossType, language=graphene.String(
        description="language in which we want glosses"),
                            concept=graphene.Int(description="concept which gloss should have"),
                            name=graphene.String(description="name of gloss"),
                            tags=graphene.List(graphene.ID,
                                               description="tags which should gloss have"),
                            explicit=graphene.Boolean(True, description="True, results must fulfil"
                                                                        " all filtered arguments,"
                                                                        " False results must fulfil"
                                                                        " at least"
                                                                        " one argument "),
                            description="GET all glosses, could be filtered by language, concept, "
                                        "name or tags. If explicit is True, results must fulfil "
                                        "all filtered arguments. If explicit is False,"
                                        " results must fulfil at least one argument  "
                            )

    def resolve_glosses(self, info, language=None, concept=None, name=None, tags=None,
                        explicit=True):
        """
        GET all glosses, could be filtered by language, concept, name or tags

        :param info:
        :param language: language in which we want glosses
        :param concept: concept which gloss should have
        :param name: name of gloss
        :param tags: tags which should gloss have
        :param explicit: True, results must fulfil all filtered arguments,
         False results must fulfil at least one argument
        :return: filtered gloss
        """
        logger.info("Get all glosses filtered by language: %s, concept: %s, \
                    name: %s, tags: %s was \
                    called %s", language, concept, name, tags, user_info(info))
        terms = get_terms(explicit=explicit, language=language,
                          concept=concept, name=name, tags=tags)

        return create_glosses_array(terms)

    gloss = graphene.Field(GlossType, language=graphene.String(required=True,
                                                               description="language of gloss"),
                           concept=graphene.Int(required=True, description="concept of gloss"),
                           description="GET specific gloss by language and concept")

    def resolve_gloss(self, info, language, concept):
        """
        GET specific gloss by language and concept

        :param info:
        :param language: language of gloss
        :param concept:  concept of gloss
        :return: found gloss
        """
        logger.info("Get gloss by language: %s and concept: %s was called %s",
                    language, concept, user_info(info))
        lang = Language.objects.get(name__iexact=language)
        concept = Concept.objects.get(pk=concept)
        term = Term.objects.get(language=lang, concept=concept)
        defin = Definition.objects.filter(language=lang, concept=concept)
        return GlossType(term, defin)

    # ================================== Others ==========================================

    term = graphene.Field(TermType, id=graphene.ID(required=True, description="id of term"),
                          description="GET term by id")
    terms = graphene.List(TermType, description="GET all terms")

    def resolve_terms(root, info, **kwargs):
        """
        Get all terms

        :param info:
        :param kwargs:
        :return: all terms
        """
        logger.info("Get all terms was called %s", user_info(info))
        return Term.objects.all()

    def resolve_term(root, info, id):
        """
        GET specific term

        :param info:
        :param id: of term
        :return: term
        """
        logger.info("Get term by id: %s was called %s", id, user_info(info))
        return Term.objects.get(pk=id)

    languages = graphene.List(LanguageType, description=" GET all languages ")
    language = graphene.Field(LanguageType, name=graphene.String(required=True,
                                                                 description="language name"),
                              description=" GET language by name")

    def resolve_languages(root, info, **kwargs):
        """
        GET all languages
        :param info:
        :param kwargs:
        :return: all languages
        """
        logger.info("Get all languages was called %s", user_info(info))
        return Language.objects.all()

    def resolve_language(root, info, name):
        """
        GET specific language by name

        :param info:
        :param name: of language
        :return: language
        """
        logger.info("Get language by name: %s was called %s", name, user_info(info))
        return Language.objects.filter(name__iexact=name).get()

    tags = graphene.List(TagTagType, description=" GET all tags")
    tag = graphene.Field(TagTagType, name=graphene.String(required=True, description="name of tag"),
                         description="GET tag by name")

    def resolve_tags(root, info, **kwargs):
        """
        GET all tags

        :param info:
        :param kwargs:
        :return: all tags
        """
        logger.info("Get all tags was called %s", user_info(info))
        return Tag.objects.all()

    def resolve_tag(root, info, name):
        """
        GET tag by name
        :param info:
        :param name: of tag
        :return: tag
        """
        logger.info("Get tag by name: %s was called %s", name, user_info(info))
        return Tag.objects.get(pk=name)

    tag_types = graphene.List(TagTypeType, description="GET all tags types")
    tag_type = graphene.Field(TagTypeType, name=graphene.String(required=True,
                                                                description="name of tag"),
                              description="GET tag type by name")

    def resolve_tag_types(root, info, **kwargs):
        """
        GET all tag types

        :param info:
        :param kwargs:
        :return: all tag types
        """
        logger.info("Get all tag types was called %s", user_info(info))
        return TagType.objects.all()

    def resolve_tag_type(root, info, name):
        """
        GET tag type by name

        :param info:
        :param name: of tag type
        :return: specific tag type
        """
        logger.info("Get tag by type: %s was called %s", name, user_info(info))
        return TagType.objects.get(name__iexact=name)

    definitions = graphene.List(DefinitionType, description="GET all definitions")
    definition = graphene.Field(DefinitionType, id=graphene.ID(required=True,
                                                               description="id of definition"),
                                description="GET definition by id")

    def resolve_definitions(root, info, **kwargs):
        """
        GET all definition

        :param info:
        :param kwargs:
        :return: all definitions
        """
        logger.info("Get all definitions was called %s", user_info(info))
        return Definition.objects.all()

    def resolve_definition(root, info, id):
        """
        GET specific definition by id

        :param info:
        :param id: of definition
        :return: specific definition
        """
        logger.info("Get definition by id: %s was called %s", id, user_info(info))
        return Definition.objects.get(pk=id)

    term_by_name = graphene.List(TermType, name=graphene.String(description="name of term"),
                                 name_startswith=graphene.String(
                                     description="string which should name stars with"),
                                 name_contains=graphene.String(
                                     description="string which should name of term countains"),
                                 description="GET all terms with some name defines by parameters")

    def resolve_term_by_name(root, info, name=None, name_startswith=None, name_contains=None):
        """
        GET all terms with some name defines by parameters

        :param info:
        :param name: name of term
        :param name_startswith: string which should name stars with
        :param name_contains: string which should name of term countains
        :return: filtered terms
        """
        terms = Term.objects.all()
        if name is not None:
            terms = terms.filter(name__iexact=name)
        if name_startswith is not None:
            terms = terms.filter(name__istartswith=name_startswith)
        if name_contains is not None:
            terms = terms.filter(name__icontains=name_contains)
        return terms


# ================================== MUTATIONS ==========================================

# ================================== Concept ==========================================
class CreateConcept(graphene.Mutation):
    """
    POST create Concept
    """
    concept = graphene.Field(ConceptType)

    @classmethod
    def mutate(cls, root, info):
        """Create concept"""
        mutate_logg(info)
        concept = Concept.objects.create()
        return CreateConcept(concept=concept)


class DeleteConcept(graphene.Mutation):
    """
    DEL delete Concept
    id represents concept
    """

    class Arguments:
        """Arguments to use for the mutation Field"""
        id = graphene.ID(required=True, description="id of concept")

    concept = graphene.Field(ConceptType)

    @classmethod
    def mutate(cls, root, info, id):
        """Delete concept, id argument is obligatory"""
        mutate_logg(info)
        concept = Concept.objects.get(pk=id)
        if concept is not None:
            concept.delete()
        return DeleteConcept(Concept(id=id))


class DeleteUnusedConcept(graphene.Mutation):
    """
    Delete all concept which are unused
    """
    concept = graphene.List(ConceptType)

    @classmethod
    def mutate(cls, root, info):
        """delete all unused Concepts"""
        mutate_logg(info)
        concepts = Concept.objects.all()
        result = []
        for conc in concepts:
            term = Term.objects.filter(concept=conc.id).exists()
            defin = Definition.objects.filter(concept=conc.id).exists()
            if not term and not defin:
                result.append(Concept(id=conc.id))
                conc.delete()
        return DeleteUnusedConcept(result)


# ================================== Language ==========================================

class CreateLanguage(graphene.Mutation):
    """
    POST create language
    , name is two letter code for language
    """

    class Arguments:
        """Arguments to use for the mutation Field"""
        name = graphene.String(required=True, description="name of language, two letter code")

    language = graphene.Field(LanguageType)

    @classmethod
    def mutate(cls, root, info, name):
        """ create language name argument is obligatory"""
        mutate_logg(info)
        language = Language(name=name)
        language.save()
        return CreateLanguage(language)


class DeleteLanguage(graphene.Mutation):
    """
    DEL Delete language if exists
    """

    class Arguments:
        """Arguments to use for the mutation Field"""
        name = graphene.String(required=True, description="name of language, two letter code")

    language = graphene.Field(LanguageType)

    @classmethod
    def mutate(cls, root, info, name):
        """delete language, name argument is obligatory """
        mutate_logg(info)
        language = Language.objects.get(name=name)
        if language is not None:
            language.delete()
        return DeleteLanguage(Language(name=name))


# ================================== TagType ==========================================

class CreateTagType(graphene.Mutation):
    """
    POST create tag type
    """

    class Arguments:
        """Arguments to use for the mutation Field"""
        name = graphene.String(required=True, description="name of tag type")

    tag_type = graphene.Field(TagTypeType)

    @classmethod
    def mutate(cls, root, info, name):
        """ create tag type, name argument is obligatory """
        mutate_logg(info)
        tag_type = TagType.objects.create(name=name)
        return CreateTagType(tag_type)


class UpdateTagType(graphene.Mutation):
    """
    PUT update tag type name,
    param new_name is new tag name,
    name represents tagType
    """

    class Arguments:
        """Arguments to use for the mutation Field"""
        name = graphene.String(required=True, description="old name of tag type")
        new_name = graphene.String(required=True, description="new name of tag type")

    tag_type = graphene.Field(TagTypeType)

    @classmethod
    def mutate(cls, root, info, name, new_name):
        """ PUT update tag type name
        new_name: is new tag name
        name:represents tagtype
        """
        mutate_logg(info)
        tag_type = TagType.objects.get(name__iexact=name)

        if tag_type is not None and not TagType.objects.filter(name__iexact=new_name).exists():
            # tag_type.name = new_name
            try:
                tag_type = TagType.objects.create(name=new_name)
                tags = Tag.objects.filter(tag_type__name=name)
                for tag in tags:
                    tag.tag_type = tag_type
                    tag.save()
                TagType.objects.get(name__iexact=name).delete()
            except Exception as exc:
                logger.warning("Tag Type can not be saved. Exception message: %s", str(exc))
        return UpdateTagType(tag_type)


class DeleteTagType(graphene.Mutation):
    """
    DEL Delete tag type
    name represents tagType
    """

    class Arguments:
        """Arguments to use for the mutation Field"""
        name = graphene.String(required=True, description="name of tag type")

    tag_type = graphene.Field(TagTypeType)

    @classmethod
    def mutate(cls, root, info, name):
        """delete tag type
        :name represents tag type"""
        mutate_logg(info)
        tag_type = TagType.objects.get(name__iexact=name)
        old = TagType(name=tag_type.name)
        if tag_type is not None:
            tag_type.delete()
        return UpdateTagType(old)


# ================================== Tag ==========================================


class CreateTag(graphene.Mutation):
    """
     POST create tag,
      name of tag,
      tag_type name of type"""

    class Arguments:
        """Arguments to use for the mutation Field"""
        name = graphene.String(required=True, description="name of tag")
        tag_type = graphene.String(required=True, description="name of tag type")

    tag = graphene.Field(TagTagType)

    @classmethod
    def mutate(cls, root, info, name, tag_type):
        """create tag
        :name of tag
        :tag_type for tag"""
        mutate_logg(info)
        tag = Tag.objects.create(name=name, tag_type=TagType.objects.filter(
            name__iexact=tag_type).get())
        return CreateTag(tag)


class UpdateTag(graphene.Mutation):
    """ PUT update tag name or type
        name represents id/name of tag
        name is new tag name
        tag_type is new type of tag"""

    class Arguments:
        """Arguments to use for the mutation Field"""
        name = graphene.String(required=True, description="old name of tag")
        new_name = graphene.String(description="new name of tag")
        tag_type = graphene.ID(description="name of tag type")

    tag = graphene.Field(TagTagType)

    @classmethod
    def mutate(cls, root, info, name, new_name=None, tag_type=None):
        """ update tag name or type
        :name represents tag
        :new_name is new tag name
        :tag_type is new type"""
        mutate_logg(info)
        tag = Tag.objects.get(pk=name)
        if tag is not None and not Tag.objects.filter(
                name__iexact=new_name).exists():
            tag.name = new_name if new_name is not None else tag.name
            tag.tag_type = TagType.objects.filter(
                name__iexact=tag_type).get() if tag_type is not None else tag.tag_type
            try:
                tag.save()
                if new_name is not None:
                    terms = Term.objects.filter(tags__name=name)
                    for term in terms:
                        term.tags.remove(name)
                        term.tags.add(new_name)
                        term.save()
                    Tag.objects.get(name__iexact=name).delete()
            except Exception as exc:
                logger.warning("Tag can not be saved. Exception message: %s", str(exc))
        return UpdateTag(tag)


class DeleteTag(graphene.Mutation):
    """
    DEL Delete tag,
     name represents tag
    """

    class Arguments:
        """Arguments to use for the mutation Field"""
        name = graphene.String(required=True, description="name of tag")

    tag = graphene.Field(TagTagType)

    @classmethod
    def mutate(cls, root, info, name):
        """
        delete tag
        :param name: represents tag
        """
        mutate_logg(info)
        tag = Tag.objects.get(pk=name)
        old = Tag(name=tag.name, tag_type=tag.tag_type)
        if tag is not None:
            tag.delete()
        return DeleteTag(old)


# ================================== Definition ==========================================


class CreateDefinition(graphene.Mutation):
    """
    POST create definition,
    file: describe definition, concept and language must be same as in term
    abstract: brief overview of definition"""

    class Arguments:
        """Arguments to use for the mutation Field"""
        file = graphene.String(required=True, description="text definition for gloss")
        language = graphene.String(required=True, description="language of definition")
        concept = graphene.ID(required=True, description="concept of definition")
        abstract = graphene.String(description="abstract of definition/gloss")

    definition = graphene.Field(DefinitionType)

    @classmethod
    def mutate(cls, root, info, file, language, concept, abstract=None):
        """
        POST create definition
        :param file: to describe definition
        :param language: of definition
        :param concept: of definition
        :param abstract: brief overview of definition
        """
        mutate_logg(info)
        if abstract is None:
            try:
                defin = Definition.objects.create(file=file,
                                                  language=Language.objects.filter(
                                                      name__iexact=language).get(),
                                                  concept=Concept.objects.get(pk=concept)
                                                  )
            except Exception as exc:
                logger.error(" Definition cannot be created, message: %s", str(exc))
                return CreateDefinition(Exception(str(exc)))
        else:
            try:
                defin = Definition.objects.create(file=file,
                                                  language=Language.objects.filter(
                                                      name__iexact=language).get(),
                                                  concept=Concept.objects.get(pk=concept),
                                                  abstract=abstract)
            except Exception as exc:
                logger.error(" Definition cannot be created, message: %s", str(exc))
                return CreateDefinition(Exception(str(exc)))
        return CreateDefinition(defin)


class UpdateDefinition(graphene.Mutation):
    """
    PUT update definition,id represents definition
    file: describe definition, concept and language must be same as in term
    abstract: brief overview of definition
    """

    class Arguments:
        """Arguments to use for the mutation Field"""
        id = graphene.ID(required=True, description="id of definition")
        file = graphene.String(description="text definition for gloss")
        language = graphene.String(description="language of definition")
        concept = graphene.ID(description="concept of definition")
        abstract = graphene.String(description="abstract of definition/gloss")

    definition = graphene.Field(DefinitionType)

    @classmethod
    def mutate(cls, root, info, id, file=None, language=None, concept=None, abstract=None):
        """
        Update definition
        :param id: represents definition
        :param file: to describe definition
        :param language: of definition
        :param concept: of definition
        :param abstract: brief overview of definition
        """
        mutate_logg(info)
        defin = Definition.objects.get(pk=id)
        defin.file = file if file is not None else defin.file
        defin.language = Language.objects.filter(name__iexact=language).get() \
            if language is not None else defin.language
        defin.concept = Concept.objects.get(pk=concept) if concept is not None else defin.concept
        defin.abstract = abstract if abstract is not None else defin.abstract
        try:
            defin.save()
        except Exception as exc:
            logger.error(" Definition cannot be saved, message: %s", str(exc))
            return UpdateDefinition(Exception(str(exc)))
        return UpdateDefinition(defin)


class DeleteDefinition(graphene.Mutation):
    """
    DEL delete definition,
     id represents definition
    """

    class Arguments:
        """Arguments to use for the mutation Field"""
        id = graphene.ID(required=True, description="id of definition")

    definition = graphene.Field(DefinitionType)

    @classmethod
    def mutate(cls, root, info, id):
        """
        delete definition
        :param id: represents definition
        """
        mutate_logg(info)
        defin = Definition.objects.get(pk=id)
        if defin is not None:
            old = Definition(pk=defin.pk, file=defin.file, abstract=defin.abstract,
                             concept=defin.concept, language=defin.language)
            defin.delete()
            return DeleteDefinition(old)
        return DeleteDefinition(defin)


# ================================== Term ==========================================
def set_tags(tags):
    """
     Find tags according to id, append them to list

    :param tags: id of wanted tags
    :return: list of really existing tags
    """
    tag_set = []
    for tag_id in tags:
        try:
            tag_object = Tag.objects.get(pk=tag_id)
            tag_set.append(tag_object)
        except Exception:
            logger.warning("This tag: %s doesnt exists", tag_id)
    return tag_set


def try_save_term(term, tags):
    """
    Try save term and add tags to term.

    :param term: to be saved
    :param tags: to be added
    :return: term and True for success, exception and False for failure
    """
    try:
        term.save()
    except ValidationError as error:
        if error.code in ("no_tag", "name_lang_tag") and tags is not None:
            term.force_save()
            logger.warning("Term: %d has no tag", term.pk)
        else:
            logger.error("Term cannot be save, message: %s", str(error.message))
            return Exception(str(error.message)), False
    if tags is not None:
        term.tags.set(set_tags(tags))
        try:
            term.save()
        except Exception as exc:
            logger.error("Term cannot be save, message: %s", str(exc))
            return Exception(str(exc)), False
    return term, True


class CreateTerm(graphene.Mutation):
    """
    POST create term. concept and language: are same as in definition,

    name: of term,
    tags: array of id of tags
    """

    class Arguments:
        """Arguments to use for the mutation Field"""
        concept = graphene.ID(required=True, description="concept of term")
        name = graphene.String(required=True, description="name of term")
        language = graphene.String(required=True, description="language of term")
        tags = graphene.List(graphene.ID, description="tags for term")

    term = graphene.Field(TermType)

    @classmethod
    def mutate(cls, root, info, concept, name, language, tags=None):
        """
        Create term
        :param concept: of term
        :param name: of term
        :param language: of term
        :param tags: for term
        """
        mutate_logg(info)
        new_term = Term(name=name, language=Language.objects.filter(name__iexact=language).get(),
                        concept=Concept.objects.get(pk=concept))

        result, _ = try_save_term(new_term, tags)
        return CreateTerm(result)


class UpdateTerm(graphene.Mutation):
    """
    PUT update term

     id represents term,
     name: new name,
     tags: new tags
     """

    class Arguments:
        """Arguments to use for the mutation Field"""
        id = graphene.ID(required=True, description="id of term")
        name = graphene.String(description="name of term")
        tags = graphene.List(graphene.ID, description="tags for term")

    term = graphene.Field(TermType)

    @classmethod
    def mutate(cls, root, info, id, name=None, tags=None):
        """
        PUT update term

        :param id: represents term
        :param name: new name
        :param tags: new tags
        """
        mutate_logg(info)
        term = Term.objects.get(pk=id)
        term.name = name if name is not None else term.name
        result, _ = try_save_term(term, tags)
        return UpdateTerm(result)


class DeleteTerm(graphene.Mutation):
    """
    DEL delete term,
    id represents term
    """

    class Arguments:
        """Arguments to use for the mutation Field"""
        id = graphene.ID(required=True, description="id of term")

    term = graphene.Field(TermType)

    @classmethod
    def mutate(cls, root, info, id):
        """
         Delete term

        :param root:
        :param info:
        :param id: represents term
        """
        mutate_logg(info)
        term = Term.objects.get(pk=id)
        if term is not None:
            old = Term(name=term.name, language=term.language, concept=term.concept)
            term.delete()
            return DeleteDefinition(old)
        return DeleteDefinition(term)


class CreateGloss(graphene.Mutation):
    """ POST create new gloss. This create term, definition and concept if needed.

    name: of gloss(term), language and concept represents gloss,
     file: describe gloss, abstract: brief overview of definition, tags: to division """

    class Arguments:
        """Arguments to use for the mutation Field"""
        concept = graphene.ID(description="concept of gloss")
        name = graphene.String(required=True, description="name of gloss")
        language = graphene.String(required=True, description="language of gloss")
        file = graphene.String(required=True, description="description/definition of gloss")
        abstract = graphene.String(description="abstract for gloss")
        tags = graphene.List(graphene.ID, description="tags for gloss")


    term = graphene.Field(TermType)
    definition = graphene.Field(DefinitionType)

    @classmethod
    def mutate(cls, root, info, name, language, file, abstract=None, tags=None, concept=None):
        """
        create gloss
        :param name: of gloss
        :param language: of gloss
        :param file: to describe gloss
        :param abstract: brief overview of definition
        :param tags: for gloss
        :param concept: of gloss
        """
        mutate_logg(info)
        lang = Language.objects.filter(name__iexact=language).get()
        if concept is None:
            concept = Concept.objects.create()
            concept = concept.id
        new_term = Term(name=name, language=lang,
                        concept=Concept.objects.get(pk=concept))

        result, valid = try_save_term(new_term, tags)
        if valid:
            try:
                if abstract is not None:
                    defin = Definition.objects.create(file=file, language=lang,
                                                      concept=Concept.objects.get(pk=concept),
                                                      abstract=abstract)
                else:
                    defin = Definition.objects.create(file=file, language=lang,
                                                      concept=Concept.objects.get(pk=concept))
            except Exception as exc:
                logger.error(" Definition cannot be created, message:  %s", str(exc))
                return CreateGloss(result, Exception(str(exc)))
            return CreateGloss(result, defin)
        logger.error(" Term cannot be created, message: %s", str(result))
        return CreateGloss(result)


class UpdateGloss(graphene.Mutation):
    """
    PUT Update gloss. This update term or definition.

    Args:
        name: of gloss(term), language and concept represents gloss,
        file: describe gloss, abstract: brief overview of definition,
        tags: to division
        """

    class Arguments:
        """Arguments to use for the mutation Field"""
        concept = graphene.ID(required=True, description="concept of gloss")
        name = graphene.String(description="name of gloss")
        language = graphene.String(required=True, description="old language of gloss")
        new_language = graphene.String(description="new language of gloss")
        file = graphene.String(description="description/definition of gloss")
        abstract = graphene.String(description="abstract for gloss")
        tags = graphene.List(graphene.ID, description="tags for gloss")


    term = graphene.Field(TermType)
    definition = graphene.Field(DefinitionType)

    def mutate(cls, info, concept, language, new_language=None, name=None, file=None,
               abstract=None, tags=None):
        """
        update gloss
        :param concept: represents gloss
        :param language: represents gloss
        :param new_language: to change te old one
        :param name: new name of gloss
        :param file: new description for gloss
        :param abstract: new brief overview  for gloss
        :param tags: new tags
        """
        mutate_logg(info)
        term = Term.objects.filter(language__name__iexact=language, concept_id=concept).get()
        term.name = name if name is not None else term.name
        term.language = Language.objects.filter(name__iexact=new_language).get() \
            if new_language is not None else term.language

        result, valid = try_save_term(term, tags)
        if valid:
            defin = Definition.objects.filter(language__name__iexact=language,
                                              concept_id=concept)
            if not defin.exists():
                defin = Definition.objects.create(language=Language.objects.get(
                    name__iexact=language), concept=Concept.objects.get(pk=concept), file="")
            else:
                defin = defin.first()
            defin.file = file if file is not None else defin.file
            defin.language = Language.objects.filter(
                name__iexact=new_language).get() \
                if new_language is not None else defin.language
            defin.abstract = abstract if abstract is not None else defin.abstract
            try:
                defin.save()
            except Exception as exc:
                logger.error(" Definition cannot be saved, message:  %s", str(exc))
                return UpdateGloss(result, Exception(str(exc)))
            return UpdateGloss(result, defin)
        logger.error(" Term cannot be saved, message:  %s", str(result))
        return UpdateGloss(result)


class DeleteGloss(graphene.Mutation):
    """
    DEL Delete gloss. Delete term and it's definition.
    Concept and language represents gloss"""

    class Arguments:
        """Arguments to use for the mutation Field"""
        concept = graphene.ID(required=True, description="concept for gloss")
        language = graphene.String(required=True, description="language for gloss")

    term = graphene.Field(TermType)
    definition = graphene.Field(DefinitionType)

    @classmethod
    def mutate(cls, root, info, language, concept):
        """
        DEL delete gloss
        :param language: represents gloss
        :param concept: represents gloss
        """
        mutate_logg(info)
        lang = Language.objects.filter(name__iexact=language).get()
        term = Term.objects.filter(language=lang, concept_id=concept).get()
        defin = Definition.objects.filter(language=lang, concept_id=concept).first()
        if term is not None:
            term.delete()
            return DeleteGloss(term, defin)
        return DeleteGloss(term)


class UpdateDefinitionTags(graphene.Mutation):
    """
    UPDATE all pre-callculated tags.
    when lemmatize.get_main_data function is changed this
    update all pre-callculated tags
    """
    definition = graphene.Field(DefinitionType)

    @classmethod
    def mutate(cls, root, info):
        """ UPDATE all pre-callculated tags """
        defin = Definition.objects.all()
        for i in defin:
            logger.info("old lemm_tags: %s \n", i.lemm_tags)
            i.lemm_tags = get_main_data(i.language.name, i.file)
            i.save()
            logger.info("new lemm_tags: %s \n", i.lemm_tags)
        return UpdateDefinitionTags(defin)


class Mutation(graphene.ObjectType):
    """ Operate Create update and delete operations """
    create_concept = CreateConcept.Field()
    delete_concept = DeleteConcept.Field()
    delete_unused_concepts = DeleteUnusedConcept.Field()

    create_language = CreateLanguage.Field()
    delete_language = DeleteLanguage.Field()

    create_tag_type = CreateTagType.Field()
    update_tag_type = UpdateTagType.Field()
    delete_tag_type = DeleteTagType.Field()

    create_tag = CreateTag.Field()
    update_tag = UpdateTag.Field()
    delete_tag = DeleteTag.Field()

    create_definition = CreateDefinition.Field()
    update_definition = UpdateDefinition.Field()
    delete_definition = DeleteDefinition.Field()

    create_term = CreateTerm.Field()
    update_term = UpdateTerm.Field()
    delete_term = DeleteTerm.Field()

    create_gloss = CreateGloss.Field()
    update_gloss = UpdateGloss.Field()
    delete_gloss = DeleteGloss.Field()

    update_definition_tags = UpdateDefinitionTags.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
