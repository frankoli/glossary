""" Testing api """
# pylint: skip-file
from django.core.management import call_command
from snapshottest import TestCase
from django.test.testcases import TransactionTestCase

from glossary.api.schema import schema
from graphene.test import Client

from glossary.models import TagType, Concept, Language, Tag, Term, Definition


class CreateTests(TransactionTestCase, TestCase):
    reset_sequences = True

    def setUp(self):
        TagType.objects.all().delete()
        Tag.objects.all().delete()
        Concept.objects.all().delete()
        Language.objects.all().delete()
        Term.objects.all().delete()
        Definition.objects.all().delete()

    def test_create_concept(self):
        """Testing the API for createConcept"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''mutation{createConcept{concept{ id }}}'''))

    def test_create_tag_type(self):
        client = Client(schema)
        self.assertMatchSnapshot(client.execute(
            '''mutation{createTagType(name: "basic"){tagType{name}}}'''))
        assert (TagType.objects.filter(name="basic").exists())

    def test_create_tag(self):
        TagType.objects.create(name="basic")
        client = Client(schema)
        self.assertMatchSnapshot(client.execute(
            '''mutation{createTag(name: "test", tagType: "basic"){tag{name}}}'''))
        assert (Tag.objects.filter(name="test").exists())

    def test_tag_without_type(self):
        """tag without tagtype cannot create"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute(
            '''mutation{createTag(name: "test", tagType: "basic"){tag{name}}}'''))
        assert (not Tag.objects.filter(name="test").exists())

    def test_create_language(self):
        client = Client(schema)
        self.assertMatchSnapshot(client.execute(
            '''mutation{createLanguage(name: "cz"){language{name}}}'''))
        assert (Language.objects.filter(name__exact="cz").exists())

    def test_no_two_languages(self):
        client = Client(schema)
        self.assertMatchSnapshot(client.execute(
            '''mutation{createLanguage(name: "cz"){language{name}}}'''))
        assert (Language.objects.filter(name__exact="cz").exists())
        self.assertMatchSnapshot(client.execute(
            '''mutation{createLanguage(name: "cz"){language{name}}}'''))
        assert (Language.objects.filter(name__exact="cz").exists())

    def test_create_term(self):
        client = Client(schema)
        concept = Concept.objects.create()
        Language.objects.create(name="en")
        tag_type = TagType.objects.create(name="basic")
        Tag.objects.create(name="test", tag_type=tag_type)
        Tag.objects.create(name="szz", tag_type=tag_type)
        self.assertMatchSnapshot(
            client.execute('''mutation{createTerm(concept: ''' + str(concept.id) + ''',
         language: "en", name: "cat", tags: ["test", "szz", "asdfghj"]){
            term{language{name},name,tags{name}}}}'''))

    def test_create_definition(self):
        client = Client(schema)
        concept = Concept.objects.create()
        lang = Language.objects.create(name="en")
        self.assertMatchSnapshot(client.execute('''mutation{
          createDefinition(concept:  ''' + str(concept.id) + ''', file: "some file", language: "en",
           abstract: "abstract"){definition{language{name},file,abstract}}}'''))
        assert (not Definition.objects.filter(file__exact="some file").exists())
        term = Term(name="test", concept=concept, language=lang)
        term.save()
        self.assertMatchSnapshot(client.execute('''mutation{
  createDefinition(concept:  ''' + str(concept.id) + ''', file: "some file", language: "en",
   abstract: "abstract"){definition{language{name},file,abstract}}}'''))
        assert (Definition.objects.filter(file__exact="some file").exists())

    def test_create_gloss(self):
        client = Client(schema)
        Language.objects.create(name="en")
        tag_type = TagType.objects.create(name="basic")
        Tag.objects.create(name="test", tag_type=tag_type)
        self.assertMatchSnapshot(client.execute(
            '''mutation{
                        createGloss(name: "kniha", file: "vec na cteni", abstract: "abstract"
                              language: "en", tags: ["test", "asdfghj"] ){
                                term{
                                  name,
                                  tags{
                                    name
                                  }
                                }
                                definition{
                                  file
                                }
                                
                              }
                    }'''
        ))
        assert (Term.objects.filter(name="kniha").exists())
        assert (Definition.objects.filter(file="vec na cteni").exists())
        assert (Definition.objects.filter(file="vec na cteni").get().abstract == "abstract")


class UpdateTests(TransactionTestCase, TestCase):
    reset_sequences = True

    def setUp(self):
        tag_type = TagType.objects.create(name="basic")
        tag = Tag.objects.create(name="test", tag_type=tag_type)
        lang = Language.objects.create(name="cz")
        concept = Concept.objects.create()
        term = Term(name="test", language=lang, concept=concept)
        term.save()
        term.tags.add(tag)
        term.save()
        Definition.objects.create(abstract="abstract", file="test 123", concept=concept,
                                  language=lang)

    def test_update_tag_type(self):
        client = Client(schema)
        self.assertMatchSnapshot(client.execute(
            '''mutation{updateTagType(name: "basic", newName: "admin"){tagType{name}}}'''))

        assert (TagType.objects.filter(name="admin").exists())
        assert (not TagType.objects.filter(name="basic").exists())
        assert (Tag.objects.filter(name__exact="test").get().tag_type_id == "admin")

    def test_update_tag(self):
        client = Client(schema)
        TagType.objects.create(name="admin")
        term = Term.objects.filter(name__exact="test").get()
        assert (str(term.tags.get()) == "test")
        self.assertMatchSnapshot(client.execute(
            '''mutation{updateTag(name: "test", newName: "pst"){tag{name}}}'''))
        assert (Tag.objects.filter(name="pst").exists())
        assert (not Tag.objects.filter(name="test").exists())
        assert (Term.objects.filter(name__exact="test").exists())
        term = Term.objects.filter(name__exact="test").get()
        assert (str(term.tags.get()) == "pst")
        self.assertMatchSnapshot(client.execute(
            '''mutation{updateTag(name: "pst", tagType: "admin"){tag{name, tagType{name}}}}'''))

        assert (Tag.objects.filter(name="pst").get().tag_type_id == "admin")
        assert (Term.objects.filter(name__exact="test").exists())
        term = Term.objects.filter(name__exact="test").get()
        assert (str(term.tags.get()) == "pst")

    def test_update_term(self):
        client = Client(schema)
        term = Term.objects.filter(name="test").get()
        self.assertMatchSnapshot(client.execute(
            '''mutation{updateTerm(id: ''' + str(term.id) + ''',
         name: "some", tags: [""]){term{name,tags{name }}}}'''))
        assert (not Term.objects.filter(name="test").exists())
        assert (Term.objects.filter(name="some").exists())

    def test_update_definition(self):
        client = Client(schema)
        defin = Definition.objects.filter(file="test 123").get()
        self.assertMatchSnapshot(client.execute(
            '''mutation{updateDefinition(id: ''' + str(defin.id) + ''', file: "some",
             abstract: "new abstract"){definition{file, abstract}}}'''
        ))
        assert (not Definition.objects.filter(file="test 123").exists())
        assert (Definition.objects.filter(file="some").exists())
        assert (Definition.objects.filter(file="some").get().abstract == "new abstract")

    def test_update_definition_term(self):
        """update language and concept"""
        client = Client(schema)
        defin = Definition.objects.filter(file="test 123").get()
        lang = Language.objects.create(name="en")
        concept = Concept.objects.create()
        term = Term(name="heloo", language=lang, concept=concept)
        self.assertMatchSnapshot(client.execute(
            '''mutation{updateDefinition(id: ''' + str(defin.id) + ''', concept: ''' + str(
                concept.id) + ''', language: "en"){definition{file, abstract, concept{id}, 
                language{name}}}}'''
        ))
        assert (
            Definition.objects.filter(language__name="cz", concept_id=defin.concept_id).exists())
        assert (not Definition.objects.filter(language__name="en",
                                              concept_id=concept.id).exists())
        term.save()
        self.assertMatchSnapshot(client.execute(
            '''mutation{updateDefinition(id: ''' + str(defin.id) + ''', concept: ''' + str(
                concept.id) + ''', language: "en"){definition{file, abstract, concept{id}, 
                        language{name}}}}'''
        ))
        assert (not Definition.objects.filter(language__name="cz",
                                              concept_id=defin.concept_id).exists())
        assert (Definition.objects.filter(language__name="en",
                                          concept_id=concept.id).exists())

    def test_update_gloss(self):
        client = Client(schema)
        term = Term.objects.filter(name="test").get()
        assert (term.tags.exists())
        self.assertMatchSnapshot(client.execute(
            '''mutation{updateGloss(concept: ''' + str(term.concept.id) + ''', language: "cz" file:
            "new file", abstract: "new abstract", name: "new name", tags: [])
            {definition{file, abstract}, term{name, tags{name}}}}'''))
        assert (not Definition.objects.filter(file="test 123").exists())
        assert (Definition.objects.filter(file="new file").exists())
        assert (Definition.objects.filter(file="new file").get().abstract == "new abstract")
        assert (not Term.objects.filter(name="test").exists())
        assert (Term.objects.filter(name="new name").exists())
        assert (not Term.objects.filter(name="new name").get().tags.exists())

    def test_update_gloss_language(self):
        client = Client(schema)
        term = Term.objects.filter(name="test").get()
        Language.objects.create(name="en")
        self.assertMatchSnapshot(client.execute(
            '''mutation{updateGloss(concept: ''' + str(term.concept.id) + ''', language: "cz",
             newLanguage: "en") {definition{file, abstract, language{name}}, term{name, 
             tags{name}, language{name}}}}'''))
        assert (not Definition.objects.filter(concept_id=term.concept.id,
                                              language_id="cz").exists())
        assert (Definition.objects.filter(concept_id=term.concept.id,
                                          language_id="en").exists())
        assert (not Term.objects.filter(concept_id=term.concept.id,
                                        language_id="cz").exists())
        assert (Term.objects.filter(concept_id=term.concept.id,
                                    language_id="en").exists())


class DeleteTests(TransactionTestCase, TestCase):
    reset_sequences = True
    concept = Concept()

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        concept1 = Concept.objects.create()
        langcz = Language.objects.create(name="cz")
        Term(name="To_Delete", language=langcz,
             concept=concept1).save()

    def setUp(self):
        self.concept = Concept.objects.create()
        if not Language.objects.filter(name="en").exists():
            langen = Language.objects.create(name="en")
        else:
            langen = Language.objects.filter(name="en").get()
        if not Language.objects.filter(name="cz").exists():
            langcz = Language.objects.create(name="cz")
        else:
            langcz = Language.objects.filter(name="cz").get()
        tag_type = TagType.objects.create(name="basic")
        tag = Tag.objects.create(name="test", tag_type=tag_type)
        term = Term(name="Testen", language=langen,
                    concept=self.concept)
        term.save()
        term.tags.add(tag)
        term.save()
        Term(name="Testcz", language=langcz,
                    concept=self.concept).save()

        Definition.objects.create(file="heloo testcz", concept=self.concept,
                                  language=langcz)
        Definition.objects.create(file="heloo testen", concept=self.concept,
                                  language=langen)

    def tearDown(self):
        TagType.objects.all().delete()
        Tag.objects.all().delete()
        Definition.objects.all().delete()
        Term.objects.exclude(name="To_Delete").delete()

    def test_delete_concept(self):
        """Testing the API for deleteConcept"""
        client = Client(schema)
        assert (Term.objects.filter(name="To_Delete").exists())
        self.assertMatchSnapshot(client.execute('''mutation{deleteConcept(id: 1){concept{ id
                }}}'''))
        assert (not Term.objects.filter(name="To_Delete").exists())

    def test_delete_gloss(self):
        """Testing the API for deleteGloss
        test that definition and term are deleted
        Test if concept is deleted when all definition and terms are deleted"""
        client = Client(schema)
        assert (Term.objects.filter(name__exact="Testen").exists())
        assert (Definition.objects.filter(file="heloo testen").exists())
        assert (Term.objects.filter(name__exact="Testcz").exists())
        assert (Definition.objects.filter(file="heloo testcz").exists())
        assert (Concept.objects.filter(pk=self.concept.id).exists())
        self.assertMatchSnapshot(client.execute('''mutation{deleteGloss(concept: '''
                                                + str(self.concept.id) + ''',language: "cz" )
                                                {term{ name }}}'''))
        assert (not Term.objects.filter(name__exact="Testcz").exists())
        assert (not Definition.objects.filter(file="heloo testcz").exists())
        assert (Concept.objects.filter(pk=self.concept.id).exists())
        self.assertMatchSnapshot(client.execute('''mutation{deleteGloss(concept: '''
                                                + str(self.concept.id) + ''',language: "en" )
                                                {term{ name }}}'''))
        assert (not Term.objects.filter(name__exact="Testen").exists())
        assert (not Definition.objects.filter(file="heloo testen").exists())
        assert (not Concept.objects.filter(pk=self.concept.id).exists())

    def test_delete_term(self):
        """Test if definition is deleted when term is deleted"""
        client = Client(schema)
        term = Term.objects.filter(name="Testcz").first()
        self.assertMatchSnapshot(client.execute('''mutation{deleteTerm(id: ''' + str(term.id) + ''')
        {term{name}}}'''))
        assert (not Term.objects.filter(name="Testcz").exists())
        assert (not Definition.objects.filter(file="heloo testcz").exists())

    def test_delete_definition(self):
        """Test if term is not deleted when definition is deleted"""
        client = Client(schema)
        defin = Definition.objects.filter(file="heloo testcz").first()
        self.assertMatchSnapshot(client.execute('''mutation{deleteDefinition(id: ''' + str(defin.id)
                                                + '''){definition{file}}}'''))
        assert (Term.objects.filter(name="Testcz").exists())
        assert (not Definition.objects.filter(file="heloo testcz").exists())

    def test_delete_tag(self):
        """Test if definition and term is not deleted when tag is deleted"""
        client = Client(schema)
        assert (Term.objects.filter(name="Testen").first().tags.exists())
        self.assertMatchSnapshot(client.execute('''mutation{deleteTag(name: "test"){tag{name}}}'''))
        assert (not Term.objects.filter(name="Testen").first().tags.exists())
        assert (Term.objects.filter(name="Testen").exists())
        assert (not Tag.objects.filter(name="test").exists())
        assert (Definition.objects.filter(file="heloo testen").exists())

    def test_delete_tag_type(self):
        """Test if tag is deleted when tag type is deleted"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''mutation{deleteTagType(name: "basic"){tagType{
        name}}}'''))
        assert (not TagType.objects.filter(name="basic").exists())
        assert (not Tag.objects.filter(name="test").exists())

    def test_delete_language(self):
        """Test if all definition and terms in language are deleted when language is deleted """
        client = Client(schema)
        assert (Term.objects.filter(language__name="en").exists())
        assert (Definition.objects.filter(language__name="en").exists())
        self.assertMatchSnapshot(client.execute('''mutation{deleteLanguage(name: "en"){language{ 
        name}}}'''))
        assert (not Term.objects.filter(language__name="en").exists())
        assert (not Definition.objects.filter(language__name="en").exists())


class GetTests(TransactionTestCase, TestCase):
    reset_sequences = True

    def setUp(self):
        # ---------- CONCEPT ----------
        concept1 = Concept.objects.create()
        concept2 = Concept.objects.create()
        concept3 = Concept.objects.create()
        # ---------- LANG ----------
        langcz = Language.objects.create(name="cz")
        langen = Language.objects.create(name="en")
        # ---------- TAGS ----------
        tag_type = TagType.objects.create(name="basic")
        tag_test = Tag.objects.create(name="test", tag_type=tag_type)
        tag_animal = Tag.objects.create(name="animal", tag_type=tag_type)
        # ---------- TERM ----------
        term = Term(name="Testen", language=langen, concept=concept1)
        term.save()
        term.tags.add(tag_test)
        term.save()
        Term(name="Testcz", language=langcz, concept=concept1).save()

        term = Term(name="Dog", language=langen, concept=concept2)
        term.save()
        term.tags.add(tag_animal)
        term.tags.add(tag_test)
        term.save()
        term = Term(name="Pes", language=langcz, concept=concept2)
        term.save()
        term.tags.add(tag_animal)
        term.save()
        Term(name="Be", language=langen, concept=concept3).save()
        Term(name="být", language=langcz, concept=concept3).save()

        # ---------- DEFIN ----------
        Definition.objects.create(file="heloo testcz", concept=concept1, language=langcz)
        Definition.objects.create(file="heloo testen", concept=concept1, language=langen)
        Definition.objects.create(file="Pes domácí (Canis lupus f. familiaris) je největší domestikovaná šelma a "
                                       "jedno z nejstarších domestikovaných zvířat vůbec, provázející člověka minimálně "
                                       "14 tisíc let.[1] Obecně se předpokládá, že se jedná o zdomácnělého a umělým"
                                       " výběrem změněného vlka obecného. Celosvětová populace psů je odhadována na 500"
                                       " miliónů,[2] přičemž toulavých a opuštěných psů je minimálně 370 miliónů,[2] "
                                       "nezisková organizace 600million odhaduje počet jen toulavých psů na světě právě "
                                       "na 600 miliónů zvířat.",
                                  concept=concept2, language=langcz)
        Definition.objects.create(file="The dog or domestic dog (Canis familiaris[4][5] "
                                       "or Canis lupus familiaris[5]) is a domesticated descendant of "
                                       "the wolf which is characterized by an upturning tail. The dog is derived "
                                       "from an ancient, extinct wolf,[6][7] and the modern wolf is the dog's nearest "
                                       "living relative.[8] The dog was the first species to be domesticated,[9][8] by "
                                       "hunter–gatherers over 15,000 years ago,[7] before the development of agriculture.[1]"
                                       "Due to their long association with humans, dogs have expanded to a large number of"
                                       " domestic individuals[10] and gained the ability to thrive on a starch-rich diet that "
                                       "would be inadequate for other canids.[11] Over the millennia, dogs became uniquely adapted"
                                       " to human behavior, and the human-canine bond has been a topic of frequent study.",
                                  concept=concept2, language=langen)

    def test_term_by_name_contains(self):
        """test term_by_name"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{termByName
        ( nameContains: "est" )
        {name,language{name}}}'''))

    def test_term_by_name(self):
        """test term_by_name"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{termByName
        (name: "pes")
        {name,language{name}}}'''))

    def test_term_by_name_starts(self):
        """test term_by_name"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{termByName
                (name: "pes", nameStartswith: "v")
                {name,language{name}}}'''))
        self.assertMatchSnapshot(client.execute('''query{termByName
                        (nameStartswith: "test")
                        {name,language{name}}}'''))

    def test_glosses(self):
        """test glosses"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{glosses
        (language: "cz", concept: 1, explicit: false)
        {term{name, concept{id}, language{name}}}}
        '''))
        self.assertMatchSnapshot(client.execute('''query{glosses
                (language: "cz", concept: 1, explicit: true)
                {term{name, concept{id}, language{name}}}}
                '''))

    def test_glosses_tags(self):
        """test glosses"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{glosses
                (tags: ["test", "animal"], explicit: false)
                {term{name, concept{id}, language{name}, tags{name}}}}
                '''))
        self.assertMatchSnapshot(client.execute('''query{glosses
                        (tags: ["test", "animal"], explicit: true)
                        {term{name, concept{id}, language{name}, tags{name}}}}
                        '''))

    def test_find_exact(self):
        """test find"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{find
        (term: "be", exact: true, lemmatize: false)
        {term{ name }}}
        '''))

    def test_find(self):
        """test find"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{find
                (term: "be", exact: false, lemmatize: false)
                {term{ name }}}
                '''))

    def test_find_lemmatize(self):
        """test find"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{find
                        (term: "be", exact: false, lemmatize: true)
                        {term{ name }}}
                        '''))

    def test_find_lemmatize_en(self):
        """test find with lemmatization in english language"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{find
                        (term: "be", exact: false, lemmatize: true, lang: "en")
                        {term{ name }}}'''))

    def test_find_in(self):
        """test find_In find only exact match Dog and pes"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{findIn
        (text: "Dog is pes in czech"){term{name},definition{file}}}
        '''))

    def test_find_in_cz(self):
        """test find_In find only czech words PES-psa and BÝT-bude"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{findIn
        (text: "`Chci psa` bude anglicky `I want a dog`", language: "cz"){term{name},definition{file},word}}
                '''))

    def test_find_in_en(self):
        """test find_In find only english words DOG-dogs and BE-are"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{findIn
        (text: "Dogs are psi in czech in singular pes", language: "en"){term{name},definition{
                        file}, word}}
                        '''))

    def test_gloss(self):
        """test get gloss"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{gloss(language: "cz", concept: 1)
        {term{name, language{name}, concept{id}}}}'''))

    def test_term(self):
        """test get term"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{term(id: 1){name}}'''))

    def test_terms(self):
        """test get terms"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{terms{name, language{name}}}'''))

    def test_languages(self):
        """test get languages"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{languages{name}}'''))

    def test_language(self):
        """test get language"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{language(name: "cz"){name}}'''))

    def test_tags(self):
        """test get tags"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{tags{name}}'''))

    def test_tag(self):
        """test get tag"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{tag(name: "animal"){name, tagType{name}}}'''))

    def test_tag_types(self):
        """test get tagTypes"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{tagTypes{name}}'''))

    def test_tag_type(self):
        """test get tagType"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{tagType(name: "basic"){name}}'''))

    def test_definitions(self):
        """test get definitions"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{definitions{file, language{name}}}'''))

    def test_definition(self):
        """test get definition"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''query{definition(id: 1){file, language{name}}}'''))

