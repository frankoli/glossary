""" function which are using lemmatization
"""
import logging
from collections import Counter
import simplemma
from simplemma import text_lemmatizer
from glossary import models
from glossary.settings import WORLD_COUNT
from glossary.settings import WORLD_LENGTH


def get_langdata():
    """
    Load lemmatize database for all saved languages

    :return: loaded lemmatize data
    """
    langdata = {}
    for i in models.Language.objects.all():
        try:
            if i.name == "cz":
                data = simplemma.load_data('cs')
            else:
                data = simplemma.load_data(i.name)
            langdata[i.name] = data
        except Exception:
            logging.info("Lemmatizer for language %s does not exist", i.name)
    return langdata


def get_main_data(language, text):
    """
    Get first WORLD_COUNT most common worlds in definition

    :param language: in which is definition
    :param text: of definition
    :return: founded worlds
    """
    langdata = get_langdata()
    lemmatize = text_lemmatizer(text, langdata[language])
    count = Counter(lemmatize).most_common()
    result = []
    for i in count:
        if len(i[0]) >= WORLD_LENGTH:
            result.append(i[0])
            if len(result) >= WORLD_COUNT:
                return result
    return result
