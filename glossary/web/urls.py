"""glossary URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from glossary.web import views




urlpatterns = [
    path('admin/', admin.site.urls),
    path('docs/', views.docs_view, name="docs"),
    path('', views.main_view, name="main"),
    path('tag/', views.tag_view, name="tag"),
    path('<int:concept>/', views.main_view, name="main"),
    path('error/', views.error_view, name="error"),
    # path('create/', views.add_new_view, name="create"),
    path('editor/<int:concept>/', views.editor_view, name="editor"),
    path('editor/', views.editor_view, name="editor"),
    path('delete/', views.delete_view, name="delete"),
    path('search/', views.main_search_view, name="search"),
    path('gloss/<int:concept>/', views.gloss_view, name="gloss"),
    path('gloss/', views.gloss_view, name="gloss"),




    # JS function example page
    path('example/', views.example_page_view, name="example"),
    path('example_eng/', views.example_page_view_en, name="example"),
]
