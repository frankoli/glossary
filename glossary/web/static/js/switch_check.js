// Will Bontrager Software, LLC
// https://www.willmaster.com
function ChangeCheckboxLabel(ckbx)
{
   if( ckbx.checked )
   {
      document.getElementById("my-checkbox-checked").style.display = "inline";
      document.getElementById("my-checkbox-unchecked").style.display = "none";
   }
   else
   {
     document.getElementById("my-checkbox-checked").style.display = "none";
      document.getElementById("my-checkbox-unchecked").style.display = "inline";
   }
}