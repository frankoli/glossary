function reloadStylesheets() {
        var queryString = '?reload=' + new Date().getTime();
        $('link[rel="stylesheet"]').each(function () {
            this.href = this.href.replace(/\?.*|$/, queryString);
        });
        }
/**
 * Hide all alerts
 */
function hide_alert(){
            document.getElementById("term_alert").style.display = "none";
            document.getElementById("lang_alert").style.display = "none";
            document.getElementById("cannot_create").style.display = "none";
            document.getElementById("tag_exist").style.display = "none";
            document.getElementById("tag_alert").style.display = "none";
        }

/**
 * display all alerts in array alert_type
 * @param alert_type
 */
function display_alert(alert_type){
            for (let i = 0; i < alert_type.length; i++){
                document.getElementById(alert_type[i]).style.display = "block";
                if (alert_type[i] === "term_alert") {
                   const gloss = document.querySelector('.gloss_name');
                   gloss.classList.add("invalid-input");
                }
            }
        }