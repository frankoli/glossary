/**
 * create tags string
 * remove all spaces
 * between tags past coma
 */
function  tags_to_text(param_tags, length){
     let text = "";
         for (let i = 0; i < length; i++){
             if (param_tags[i].replace(" ", "") !== "") {
                text += param_tags[i].replace(" ", "");
                if (i + 1 < length) {
                    text += ',';
                }
             }
         }
    return text;
}
/**
* create lang string, starting with &param_lang=
 * between langs are coma
* */
function lang_to_text(param_lang, length){
    let text = '&param_lang=';
    for (let i = 0; i < length; i++){
       text += param_lang[i].id;
       if (i + 1 < length){
           text +=  ',';
       }
    }
    return text;
}

/**
 * add search params to search button action
 */
function add_search_param(){
    document.getElementById("search_url").action += get_search_params();
}

/**
 * create string with all search params(language, tags)
 * @returns {string} all params
 */
function get_search_params(){
    let result = "";
    let param_lang = document.querySelectorAll( 'input[name="option-language"]:checked');
    if (param_lang.length !== 0) {
         result += lang_to_text(param_lang, param_lang.length);
    }
    // let param_tags =  document.getElementById("tags").value.split(",");
    // let length = param_tags.length;
    let param_tags = get_selected_tags()
    let text = param_tags.toString()
    let length = text.length
    if (length !== 0) {
        // let text = tags_to_text(param_tags, length);
        if (text !== ""){
            result += '&param_tags=';
            result += text;
            let exact_tags = document.getElementById("tag_switch");
            if (exact_tags.checked) {
                result += '&exact_tags=True';
            }
            else {
                result += '&exact_tags=False';
            }
        }
    }
    return result;
}