"""
This module contains all view which are using for glossary website.

Each http request can have endpoint which is created in urls.py.
When the endpoint is being specified the handlerer is specified aswell.

Django can handle the request with multiple approaches:
    Function views:
        1. Add an import:  from my_app import views
        2. Add a URL to urlpatterns:  path('', views.home, name='home')
    Class-based views
        1. Add an import:  from other_app.views import Home
        2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
    Including another URLconf
        1. Import the include() function: from django.urls import include, path
        2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import logging

from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.shortcuts import render
from django.utils.translation import gettext as _

from glossary.settings import UNDEFINED
from glossary.models import Term, Definition, Language, Concept, Tag, TagType



logger = logging.getLogger(__name__)


def render_page(request, concept, language, path, term_result=None, searched=""):
    """
    Render page specified in path

    Requirements:
        1. If term exists the page is rendered
        2. If term doesnt exist, error page will be rendered

    :param searched: whats is being search for
    :param term_result: all terms that will be rendered default=None
    :param concept: and language defines term
    :param path: to template that will be rendered
    :param language: and concept defines term
    :param request: object having information about HTTP request WSGI request
    :return: rendered page in HTML
    """
    definition = Definition.objects.filter(concept=concept, language=language)
    term_name = Term.objects.filter(concept=concept, language=language)
    if not definition.exists():
        body = _("> Definition doesnt exists yet !")
    else:
        body = definition.get()
    if term_name.exists():
        return render(request, path,
                      {'id': concept, 'lang': language, 'term': term_name.get().name, 'body': body,
                       'term_result': term_result, 'searched': searched})
    return render(request, "glossary/pages/error_page.html")


def try_save(concept, lang, *objects):
    """
    This method try save all objects passed in argument.
    If all is saved successfully, return JsonResponse with valid true.

    :return: JsonResponse with valid=true or valid=false, depending on success
    :param concept: and language defines term to save
    :param lang: concept defines term to save
    """
    for obj in objects:
        try:
            obj.save()
        except ValidationError as exc:
            logger.error(exc.message)
            return JsonResponse({"valid": False, "except": exc.code, "message": exc.message},
                                status=400), False, exc.code
    logger.info("new gloss with language: %s , concept: %s created ", str(lang), str(concept))
    return JsonResponse({"valid": True, "id": concept, "lang": lang}, status=200), True, ""


def filter_exact_tags(searched, param_tags, param_lang=None):
    """
    Filter term by searched expression, tags and language.
    Term must have exacly same param_tags.

    Hint:
        term.tags == param_tags

    :param searched: searched expression
    :param param_tags: which must be present
    :param param_lang: one of them must be present
    :return: QuerySet<Term> filtered terms
    """
    if param_lang is not None:
        result = Term.objects.filter(name__icontains=searched, language__name__in=param_lang)
    else:
        result = Term.objects.filter(name__icontains=searched)
    for tag in param_tags:
        if Tag.objects.filter(name__iexact=tag):
            result = result.filter(tags__name=tag)
    return result


def get_term_result(searched, param_lang, param_tags, exact):
    """
    Filter terms by parameters, by language, tags and searched expression

    Example:
        param_lang in term.lang and param_tags in term.lang
        if exact: term.tags == param_tags else param_tags in term.tags

    :param searched: what was searched
    :param param_lang: which languages are allowed
    :param param_tags: which tags should be present
    :param exact: if true Term must be present all tags
    :return: QuerySet<Term> filtered terms
    """
    if param_lang is not None and param_tags is None:
        return Term.objects.filter(name__icontains=searched, language__name__in=param_lang)

    if param_lang is not None and param_tags is not None:
        if exact == 'False':
            return Term.objects.filter(name__icontains=searched, language__name__in=param_lang,
                                       tags__name__in=param_tags)
        return filter_exact_tags(searched, param_tags, param_lang)

    if param_lang is None and param_tags is not None:
        if exact == 'False':
            return Term.objects.filter(name__icontains=searched, tags__name__in=param_tags)
        return filter_exact_tags(searched, param_tags)
    return Term.objects.filter(name__icontains=searched)


def get_def_result(searched, param_lang, param_tags):
    """
    Filter definition by parameters. Filters them with language and tags.

    Hint:
        definition.lang in param_lang and definition.tags in param_tags

    :param searched: what was searched
    :param param_lang: which languages are allowed
    :param param_tags: which tags should be present
    :return: QuerySet<Definition> filtered definitions
    """
    if param_tags is None:
        if param_lang is not None:
            return Definition.objects.filter(abstract__icontains=searched,
                                             language__name__in=param_lang) \
                .union(
                Definition.objects.filter(file__icontains=searched,
                                          language__name__in=param_lang)
            )
        return Definition.objects.filter(abstract__icontains=searched)\
            .union(
            Definition.objects.filter(file__icontains=searched))
    return []


def search_for(searched, param_lang=None, param_tags=None, exact=False):
    """
    Filter terms and definition by params and union them.

    :param searched: what was searched
    :param param_lang: which languages are allowed
    :param param_tags: which tags should be present
    :param exact: if must be present all tags
    :return: tuple[QuerySet<Term>, Term]filtered results, first result
    """
    term_result = get_term_result(searched, param_lang, param_tags, exact)
    first = term_result.first()
    term_result = set(term_result)
    if len(term_result) == len(Term.objects.all()):
        return term_result, first

    def_result = get_def_result(searched, param_lang, param_tags)

    for i in def_result:  # for each definition find term and add it to term_result
        try:
            term = Term.objects.filter(concept=i.concept, language=i.language).get()
            term_result.add(term)
            if not first:
                first = term
        except Exception as exc:
            logger.error("gloss definition concept: %s language: %s "
                         "id: %s doesnt have term, message: %s",
                         str(i.concept), str(i.language), str(i.id), str(exc))

    return term_result, first


def main_view(request, name="Glossary", concept=""):
    """
    Represents main view always display gloss Glossary if another gloss is not specified

    Template:
        - glossary/pages/main.html
        - ERROR -> glossary/pages/error_page.html

    Args
        - &query - languages to be searched for

    :param request: object having information about HTTP request
    :param name: of the gloss
    :param concept: of the gloss
    :return: render page main or error page
    """
    gloss = Term.objects.filter(name=name, tags__name="glossary").get()
    if concept != "":  # Filter terms for concept and language
        language = request.GET.get('query')
        gloss = Term.objects.filter(concept=concept, language=language).get()
    if not gloss:  # None found, select first terms
        gloss = Term.objects.all().first()
    try:
        term = gloss.name
        try:
            definition = Definition.objects.filter(concept=gloss.concept, language=gloss.language)
            definition = definition.get().file
        except Definition.DoesNotExist as exc:
            logger.info("term lang: %s, conc: %s doesnt have "
                        "definition, message: %s",
                        str(gloss.language), str(gloss.concept), str(exc))
            definition = _("> Definition doesnt exists yet !")
        term_result = Term.objects.all()
        return render(request, "glossary/pages/main.html",
                      {'term': term, 'body': definition, 'id': gloss.concept,
                       'lang': gloss.language, 'term_result': term_result})
    except Exception as exc:
        logger.error("exception in mainView: %s", str(exc))
        return render(request, "glossary/pages/error_page.html")


def add_tags(term, tags):
    """
    Add tags to specific term

    :param term: Term object in which new tag will be added
    :param tags: list[str] list of string which are declaring name of tag
    """
    term.tags.clear()
    for i in tags:
        try:
            term.tags.add(Tag.objects.get(name=i))
        except Exception as exc:
            logging.error("tag '%s' doesnt exist, message: %s", str(i), str(exc))


def editor_view(request, concept=UNDEFINED):
    """
    View for Gloss editor
            - for GET is render editor page(with or without gloss)
            - for POST is save new gloss or update old one

    Templates:
        - glossary/pages/editor.html

    Args
        - &searched - searched expression
        - &query - language to be rendered with

    :param request: object wrapping HTTP request
    :param concept: represents id of gloss with lang
    """
    if request.POST:
        term_name = request.POST.get('term_name')
        definition = request.POST.get('body')
        language = request.POST.get('lang')
        new_lang = request.POST.get('new_lang')
        abstract = request.POST.get('abstract')
        tags = request.POST.get('tags').split(",")

        if request.POST.get('id') is not None:  # update gloss
            concept = int(request.POST.get('id'))
            term = Term.objects.filter(concept=concept, language=language).get()
            if term_name.replace(" ", "") != "":
                term.name = term_name
            term.language = Language.objects.filter(name=new_lang).get()
            add_tags(term, tags)
            defin = Definition.objects.filter(concept=concept, language=language).get()
            defin.file = definition
            defin.language = Language.objects.filter(name=new_lang).get()
            defin.abstract = abstract
            response, _, _ = try_save(defin.concept.id, defin.language.name, term, defin)
            return response
        # create new gloss
        if term_name.replace(" ", "") == "":
            return JsonResponse({"valid": False, "except": "empty_name",
                                 "message": _("Gloss name should not be empty.")}, status=400)

        new_concept = Concept.objects.create()
        new_concept.save()
        new_term = Term(name=term_name, language=Language.objects.filter(name=new_lang).get(),
                        concept=new_concept)

        new_gloss = Definition(file=definition,
                               language=Language.objects.filter(name=new_lang).get(),
                               concept=new_concept, abstract=abstract)
        response, valid, exce = try_save(new_gloss.concept.id, new_gloss.language.name,
                                         new_term, new_gloss)
        if valid or (not valid and (exce in ("no_tag", "name_lang_tag"))):
            new_term.force_save()
            add_tags(new_term, tags)
            response, valid, _ = try_save(new_gloss.concept.id,
                                          new_gloss.language.name, new_term,
                                          new_gloss)
            if not valid:
                new_concept.delete()
        return response

    # Render page
    if concept == UNDEFINED:
        return render(request, "glossary/pages/editor.html", {})

    language = request.GET.get('query')
    searched = request.GET.get('searched')
    return render_page(request=request, concept=concept, language=language,
                       path="glossary/pages/editor.html", searched=searched)


def error_view(request):
    """
    Represents error page

    Templates:
        - glossary/pages/error_page.html

    """
    return render(request, "glossary/pages/error_page.html")


def gloss_view(request, concept=UNDEFINED):
    """
    Fullscreen gloss view representation

    Templates:
        glossary/pages/gloss_page.html

    Args
        - &searched - searched expression

    :param concept represents id of gloss with lang
    """
    search = request.GET.get('searched')
    if search is not None:
        term_result, first = search_for(search)
    else:
        term_result, first, search = Term.objects.all(), Term.objects.all().first(), ""

    if concept == UNDEFINED:
        if first is not None:
            return render_page(request=request, concept=first.concept, language=first.language,
                               path="glossary/pages/gloss_page.html", term_result=term_result,
                               searched=search)
        return main_view(request)

    language = request.GET.get('lang')
    return render_page(request=request, concept=concept, language=language,
                       path="glossary/pages/gloss_page.html", term_result=term_result,
                       searched=search)


def get_param_lang(request):
    """
    Gets all languages passes in &param_lang parameter in URL
        - ...&param_lang=cz,en will produce ['cz', 'en']

    Args
        - &param_lang - contains languages to parsed

    :param request: object having information about HTTP request
    :return: list[str] of all languages found in request
    """
    if request.GET.get('param_lang') is not None:
        return request.GET.get('param_lang').split(',')
    return None


def get_param_tags(request):
    """
    Read param tags from request and return array of existing tags
        - &param_tags=documentation,pa1,xxx ->[Term(Documentation), Term(PA1)], xxx will be ignored

    Args
        - &param_tags - contains all tags to be parsed

    :param request: object having information about HTTP request
    :return: tuple[QuerySet<Tag>, bool] tags as array and bool to check if all tags must be matched
    """
    if request.GET.get('param_tags') is not None:
        param_tag = [tag for tag in request.GET.get('param_tags').split(',')
                     if Tag.objects.filter(name__iexact=tag)]
        if len(param_tag) != 0:
            return param_tag, request.GET.get('exact_tags')
    return None, None


def main_search_view(request):
    """
    Representing the main and gloss view
        - Accepts
            - POST used when searching, params are passed in http request
            - GET is used after clicking on gloss preview
        - Args
            - &query - "main" or "gloss" - based on this will render correct template
    Templates:
        - GET is always glossary/pages/main.html
        - POST and &query == main  -> glossary/pages/main.html
        - POST and &query == gloss -> glossary/pages/gloss_page.html
        - ERROR                    -> glossary/pages/error_page.html


    :param request: object having information about HTTP request
    :return: rendered HTML page
    """
    gloss = Term.objects.filter(name="Glossary").get()
    term = gloss.name
    param_lang = get_param_lang(request)
    param_tags, exact = get_param_tags(request)
    if request.method == "POST":
        searched = request.POST['searched']

        term_result, first = search_for(searched, param_lang, param_tags, exact)
        if len(term_result) != 0:
            gloss = first
            term = gloss.name
        try:  # get definition for first term
            definition = Definition.objects.filter(concept=gloss.concept,
                                                   language=gloss.language).get().file
        except Definition.DoesNotExist as exc:
            logger.info("term lang: %s, conc: %s doesnt have "
                        "definition, message: %s", str(gloss.language),
                        str(gloss.concept), str(exc))
            definition = _("> Definition doesnt exists yet !")
        if request.GET.get('query') == 'main':
            return render(request, "glossary/pages/main.html",
                          {'term': term, 'body': definition, 'term_result': term_result,
                           'id': gloss.concept, 'lang': gloss.language, 'searched': searched,
                           'param_tags': param_tags, 'exact_tags': exact, 'param_lang': param_lang

                           })
        if request.GET.get('query') == 'gloss':
            return render(request, "glossary/pages/gloss_page.html",
                          {'term': term, 'body': definition, 'term_result': term_result,
                           'id': gloss.concept, 'lang': gloss.language, 'searched': searched

                           })
    elif request.GET.get('searched') is not None:
        term_result, first = search_for(request.GET.get('searched'), param_lang, param_tags, exact)
        if first is not None:
            gloss = first
        if request.GET.get('id') is not None:
            gloss = Term.objects.filter(concept=request.GET.get('id'),
                                        language=request.GET.get('lang')).get()

        term = gloss.name
        try:
            definition = Definition.objects.filter(concept=gloss.concept,
                                                   language=gloss.language).get().file
        except Definition.DoesNotExist as exc:
            logger.info("term lang: %s, conc: %s doesnt have "
                        "definition, message: %s", str(gloss.language),
                        str(gloss.concept), str(exc))
            definition = _("> Definition doesnt exists yet !")
        return render(request, "glossary/pages/main.html",
                      {'term': term, 'body': definition, 'term_result': term_result,
                       'id': gloss.concept, 'lang': gloss.language,
                       'searched': request.GET.get('searched'), 'param_tags': param_tags,
                       'exact_tags': exact, 'param_lang': param_lang})
    return render(request, "glossary/pages/error_page.html")


def delete_view(request):
    """
    Delete specific gloss based on id of Concept and Language.
    Gloss to delete is described with URL arguments

    Args:
        - &id - Concept id of gloss
        - &lang - Language of gloss
        - &from - page url to which we return after deleting

    Templates:
        - ERROR                    -> glossary/pages/error_page.html

    :param request: object having information about HTTP request
    :return: JsonResponse with valid = success -> false or true
    """
    if request.method == "POST":
        concept = request.POST['id']
        language = request.POST['lang']
        from_page = request.POST['from']
        term = Term.objects.filter(concept=concept, language=language).get()
        if term.tags.filter(name="glossary"):
            return JsonResponse({"valid": False, "message": _("This gloss can not be deleted")},
                                status=400)
        try:
            term.delete()
            return JsonResponse({"valid": True, "from": from_page}, status=200)
        except Exception as exc:
            logger.error(
                "While deleting object  %s exception was raised: %s}", term.name, str(exc))
            return JsonResponse({"valid": False, "except": exc,
                                 "message": _("Something went wrong, object can not be deleted")},
                                status=400)
    return render(request, "glossary/pages/error_page.html")


def tag_view(request):
    """
    This view manages creating of new Tag, can't create tag with already existing name

    Args:
        - &tag = name of tag to create

    Templates:
        - ERROR                    -> glossary/pages/error_page.html

    :param request: object having information about HTTP request
    :return: JsonResponse with valid = success -> false or true
    """
    if request.method == "POST":
        new_tag = request.POST['tag']
        if Tag.objects.filter(name__iexact=new_tag):
            return JsonResponse({"valid": False, "except": "tag_exist"}, status=400)
        try:
            Tag.objects.create(name=new_tag, tag_type=TagType.objects.filter(name="basic").get())
        except Exception as exc:
            logger.error("Exception in tagview: %s", str(exc))
            return JsonResponse({"valid": False, "except": "except"}, status=400)
        return JsonResponse({"valid": True}, status=200)
    return render(request, "glossary/pages/error_page.html")


def example_page_view(request):
    """
    Example page which shows usage of glossary on website of Programming course from CTU in Prague

    Templates:
        - ERROR                    -> glossary/pages/error_page.html


    :param request: object having information about HTTP request
    :return: render Example page
    """
    return render(request, "glossary_example/example.html")

def example_page_view_en(request):
    """
    Example page which shows usage of glossary on website of Programming course from CTU in Prague

    Templates:
        - ERROR                    -> glossary/pages/error_page.html


    :param request: object having information about HTTP request
    :return: render Example page
    """
    return render(request, "glossary_example/example_eng.html")

def docs_view(request):
    """
    Renders the documentation, the docs are stored in already generated HTML file.

    Templates:
        - index.html

    :param request: object having information about HTTP request
    :return: rendered page
    """
    return render(request, 'index.html')
