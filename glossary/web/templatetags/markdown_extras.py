"""@brief file for importing markdown converter"""
from django import template
from django.template.defaultfilters import stringfilter

import markdown as md

register = template.Library()


@register.filter()
@stringfilter
def markdown(value):
    """ function which helps convert Markdown to html"""
    return md.markdown(value, extensions=['markdown.extensions.fenced_code'])
