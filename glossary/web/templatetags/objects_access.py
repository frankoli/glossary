""" custom tags to get access to object in database"""
import logging

from django import template
from django.template.defaultfilters import stringfilter
from django.utils.translation import gettext as _

from glossary.settings import ABSTR_LENGTH, MAX_LENGTH
from glossary.models import Definition, Term, Language, Tag


register = template.Library()
logger = logging.getLogger(__name__)


@register.simple_tag
def get_abstract(concept, language, edit=False):
    """
    returns abstract or first few worlds of definition, if
    abstract doesnt exists.
    Returns only abstract if edit flag is True
    :param concept: defines gloss
    :param language: defines gloss
    :param edit: if true returns abstract
    :return: abstract or first few worlds of definition
    """
    try:
        definition = Definition.objects.filter(concept=concept,
                                             language=language).get()
    except Definition.DoesNotExist as exc:
        logger.info("term lang: %s, conc: %s doesnt have definition, "
                    "message: %s", str(language), str(concept), str(exc))
        return _("> Definition doesnt exists yet !")
    if definition.abstract:
        return definition.abstract
    if not edit:
        return definition.file[:ABSTR_LENGTH] + \
               ("..." if len(definition.file) > ABSTR_LENGTH else "")
    return ""


@register.simple_tag
def get_languages():
    """ :return: all languages """
    return Language.objects.all()


@register.simple_tag
def get_all_tags(page):
    """ :return: all tags"""
    if page == "editor":
        all_tags = Tag.objects.exclude(tag_type__name="admin")
    else:
        all_tags = Tag.objects.all()
    tags = list(tag.name for tag in all_tags)
    return tags


@register.filter()
@stringfilter
def my_filter(value):
    """
    replace a tag with object a tag
    :param value:
    :return:
    """
    value = value.replace("<a", "<object><a")
    value = value.replace("</a>", "</a></object>")
    return value


@register.simple_tag
def get_tags(concept, language):
    """
    :param concept: defines gloss
    :param language: defines gloss
    :return: tags for explicit gloss
    """
    term = Term.objects.filter(concept=concept, language=language).get()
    tags = term.tags.all()
    # res = ""
    tag_res = []
    for tag in tags:
        # res += tag.name
        # res += ", "
        tag_res.append(tag.name)
    # return res
    return tag_res


@register.simple_tag
def get_max_length(to):
    """
    :param to: for what we want to know length
    :return: return max length
    """
    if to == "abstract":
        return ABSTR_LENGTH
    return MAX_LENGTH
