"""Django admin for showing object in database"""
from django.contrib import admin

from glossary import models
from glossary.models import Concept, User, Language, Definition

admin.site.register(Concept)
admin.site.register(User)
admin.site.register(Language)
admin.site.register(Definition)


@admin.register(models.TagType)
class TagTypeAdmin(admin.ModelAdmin):
    """
    how should admin page show tag type
    """
    list_display = [
         "name",
    ]


@admin.register(models.Term)
class TermAdmin(admin.ModelAdmin):
    """how should admin page show term"""
    list_display = [
        "id", "name", "language", "concept"
    ]


@admin.register(models.Tag)
class TagAdmin(admin.ModelAdmin):
    """how should admin page show tag"""
    list_display = [
        "name", "tag_type"
    ]
