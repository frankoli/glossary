import sys

from django.test import TestCase


class VersionTest(TestCase):

    def test(self):
        """
        tests if the python version is greater or even than 3
        """
        self.assertGreaterEqual(sys.version_info.major, 3)
