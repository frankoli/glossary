.. Glossary documentation master file, created by
   sphinx-quickstart on Sun May  1 22:34:36 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Glossary's documentation!
====================================

.. toctree::
   :caption: Table of Contents
   :name: Glossary master guide !
   :maxdepth: 2
   :numbered:

   modules/views.rst
   modules/schemas.rst
   modules/lem_fun.rst
   modules/urls.rst
   modules/readme.rst
   modules/models.rst
