#!/bin/bash

VENV_DIR="venv_dir"
# check if python is installed
if ! hash python3; then
    echo "Python is not installed, try again after installing python3...";
    exit 1;
fi
# check for python version, atleast 3.8
ver=$(python3 -V 2>&1)
if [[ !  "$ver" =~ ^Python\ 3.(8|9|10).*$ ]]; then
    python3 -V
    echo "This script requires python 3.8 or greater";
    exit 1;
fi

# creates venv if not didnt exist
if [ ! -d "$VENV_DIR" ]; then
  python3 -m venv "$VENV_DIR";
fi

# activates the venv
source "$VENV_DIR/bin/activate";

# check if requirements.txt matches the libs installed in venv
pip freeze > tmp.txt;
if ! cmp -s "tmp.txt" "requirements.txt"; then
  pip install -r requirements.txt;
fi
rm tmp.txt

./migrate.sh
python3 manage.py runserver
