import os

import django
from django.conf import settings
from django.core.exceptions import ValidationError
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "glossary.settings")
django.setup()
from glossary.models import Term, Language, Concept, Definition, TagType, Tag


def insert_language():
    if not Language.objects.filter(name="cz").exists():
        language = Language.objects.create(name="cz")
    if not Language.objects.filter(name="en").exists():
        language = Language.objects.create(name="en")


def insert_tag_type():
    if not TagType.objects.filter(name="basic").exists():
        type = TagType(name="basic")
        type.save()
    if not TagType.objects.filter(name="admin").exists():
        type = TagType(name="admin")
        type.save()


def add_def(term):
    f = open("./README.md", "r")
    file = f.read()
    if Definition.objects.filter(language=term.language, concept=term.concept).exists():
        defin = Definition.objects.filter(language=term.language, concept=term.concept).get()
        defin.file = file
    else:
        defin = Definition(file=file, language=term.language, concept=term.concept)
    try:
        defin.save()
    except Exception as e:
        print("Readme insert failed!")


def readme_insert():
    name = "Glossary"
    if not Tag.objects.filter(name="glossary").exists():
        print(TagType.objects.all())
        tagtype = TagType.objects.all()[1]
        tag = Tag(name="glossary", tag_type=tagtype)
        try:
            tag.save()
        except Exception as e:
            print(str(e))

    if not Term.objects.filter(name=name, tags__name="glossary").exists():
        concept = Concept.objects.create()
        term = Term(name=name, language=Language.objects.filter(name="en").get(), concept=concept)
        try:
            term.save()
        except ValidationError as e:
            if e.code == "no_tag" or e.code == "name_lang_tag":
                print("Continue..")
            else:
                print("Readme insert failed!")
                return
        term.tags.add(Tag.objects.get(name="glossary"))
        try:
            term.save()
        except Exception as e:
            print("Readme insert failed!")
            return
        add_def(term)
    else:
        term = Term.objects.filter(name=name, tags__name="glossary").get()
        add_def(term)


insert_language()
insert_tag_type()
readme_insert()
