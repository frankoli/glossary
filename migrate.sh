#!/bin/bash

python3 manage.py makemigrations
python3 manage.py makemigrations glossary
python3 manage.py migrate
python3 manage.py migrate glossary
python3 insert_readme.py