# Glossary

> Work focuses on software for maintenance and
sharing knowledge and information.

> [URL](https://bc-glossary.herokuapp.com/) to deployed server on Heroku free hosting

This work focuses on software for maintenance and sharing knowledge and information.
Theore- tical part is dedicated to analyse similar software and describe the difference
between GraphQL, SOAP and REST.
Design of application was created together with whole application, which helps centralizing,
linking and keeping knowledge. Application provides API and web interface to manipulate with
glossary. Created solution supports creating records and searching in already created records.
The solution also supports searching for records in text.

New extensions can be created and easily added to the solution described above. For example 
extensions that will highlight records from glossary in text using colors and adding their 
definition to text. This extension was also implemented as a function for web source codes.

*** 

# Installation and settings
## Setup project

In settings.py are set variables `DJANGO_DATABASE` and `DJANGO_DEBUG`. 
- `DJANGO_DATABASE` decides which database will be used (production, default, test ...). You can select database from `SETTINGS.DATABASES_AVAILABLE`.
- `DJANGO_DEBUG` set debugging mode (True, False)

## Run App

- you need to install python3,  run
    
```shell
apt-get update, apt-get install python3.9 python3-venv python3-dev build-essential
```

- to install PostgreSQL run

```shell
apt-get install postgresql postgresql-contrib
```
- to run the app from glossary folder run 
```shell
./run-server.sh.
```
## Run App in docker

- firstly you need to install docker
- now you can run:
```shell
docker build . -t glossary:latest
```
which creates Docker image glossary 
- now you can run the aplication by
```shell
docker run glossary
```
    
## [Optional] Create virtual enviroment
> Following section will create folder containing raw python

`python -m venv venv`  will create venv folder with raw python
`source venv/bin/activate` will activate the enviroment

```shell
python -m venv venv
source venv/bin/activate # this will activate the enviroment
```

or 

```shell
python3 -m venv venv_dir
source venv_dir/bin/activate pip install -r requirements.txt 
# run the python app
deactivate
```
## Requirements for glossary
> requirements for project are stored in `requirements.txt`
#### Updating requirements
```shell
pip freeze > requirements.txt 
```
#### Installing requirements
```bash
pip install -r requirements.txt
```

## How to set up database
this will create migrations and migrate the to db
 
```shell
./migrate.sh 
```

create admin:

```shell
python manage.py createsuperuser
```

run insert script:
```shell
python3 insert_readme.py
```

### Set up your own database
All databases configs are stored in glossary.settings.AVAILABLE_DATABASES, new databases can be added freely.
On this example you can see 2 databases connection configurations one `default` and `my_new_database`.

```python
AVAILABLE_DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'mydatabase',
        'USER': 'mydatabaseuser',
        'PASSWORD': 'mypassword',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    },
    'my_new_database': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'mydatabase',
    }
}
```

When the server is being starter the system variable `DJANGO_DATABASE` should be set to decide which database will be used.
When `DJANGO_DATABASE=my_new_database` the `my_new_database` will be used otherwise `default` is used.


Full documentation can be found [here](https://docs.djangoproject.com/en/4.0/ref/settings/#databases)




***
## Add or update language to translate 

> Web can be translated to any language. Web is in english by default. 
> If you want to translate web to another language follow the
> instructions bellow.

Firstly run ``` django-admin makemessages -l <LANG> -i venv   ```
where LANG is code of language to which you want to translate.
These codes can be found [here](http://www.i18nguy.com/unicode/language-identifiers.html)

In `locale` folder will be created another folder named `<LANG>`.
In this folder will be file called `django.po` where you can translate messages.

After translating this messages you can run
```django-admin compilemessages```

You should see translated web page in your browser. 


***
## Logging
Currently, is logging set to log everything
(INFO, WARNING, ERROR ...) to file
and console. If you want to change it you need
to change it in `glossary/settings.py`
You can add new format style, currently there are
only style called *simple*.
```python
'formatters': {
            'simple': {
                        'format': '[ {levelname} ] ({asctime}) > '
                                  '{message}; from module: {module}'
                    },
    }
```
There are two handlers console and file. You can change,
remove or add handlers. You can change format of handler
by paste another format after `'formatter':`.
File handler write logs to file in dir *log*. Name of log
file contains today date.
```python
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            'class': 'logging.FileHandler',
            'filename': f'./logs/{datetime.date.today()}_debug.log',
            'formatter': 'simple',
        }
     }
```
Then you define which handler(from handlers above) we use and which
level is logging.
```python
    'root': {
        'handlers': ['console', 'file'],
        'level': 'INFO',
    }
```
Documentation for logging [here](https://docs.djangoproject.com/en/4.0/topics/logging/).

#### Example of complete config for logging

```python
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
            'simple': {
                        'format': '[ {levelname} ] ({asctime}) > '
                                  '{message}; from module: {module}',
                        'style': '{',
                    },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            'class': 'logging.FileHandler',
            'filename': f'./logs/{datetime.date.today()}_debug.log',
            'formatter': 'simple',
        }
     },
    'root': {
        'handlers': ['console', 'file'],
        'level': 'INFO',
    },
}
```


***

# Web usage

## Add another language to web 

> Every gloss has language, by default there are english and czech. 
> If you want to add or remove some languages follow the instructions bellow.

You can add insert script to `insert_readme` or you can do it from API.
In `insert_readme` found function `insert_language` to this function add:
```
language = Language(name="es")
language.save()
```
or 
```
Language.objects.create(name="es")
```

where name iscode for language.


## Create new gloss

After tapping to `create new` button editor page will be displayed. 
There are field to **name of gloss** which can not be empty. To **describe
gloss** use Markdown editor. You can add optional **abstract** which will
be displayed as preview of gloss. You can add **tags** to gloss, you can
choose from existing gloss, or you can create new tag. 

**To create tag** you need to use filed over the tag field. Tag must be
unique and without spaces.

When you are satisfied with your gloss you can click on save button.

### Rules for creating gloss:

- In one language could be only one gloss with some name and specific tags.
> If you have gloss *switch* in english with tags *IT* and *network*,
> you can not create another gloss *switch* with the same tags in english.
> You need to have at list one different tag or this gloss must be in another
> language

- Gloss name can not be empty

## Update gloss

Tap to pencil button next to trash bin in top right corner. 
It will display the editor of gloss in which you can edit the gloss.

## Delete gloss

Tap the trash bin button in top right corner of page



