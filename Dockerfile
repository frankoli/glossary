FROM ubuntu:latest

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get -y install python3 \
    && apt-get -y install python3-dev \
    && apt-get -y install python3-venv \
    && apt-get -y install build-essential \
    && apt-get -y install postgresql \
    && apt-get -y install postgresql-contrib \
    && apt-get -y install git

RUN mkdir -p /app/ && cd /app/ \
    && git clone https://gitlab.fit.cvut.cz/frankoli/glossary.git

WORKDIR /app/glossary/

CMD ./run-server.sh